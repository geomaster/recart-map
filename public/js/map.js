var recartMap = {
    "2030110": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "PILAR QUADRADO DE MURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1070103": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDES LOCAIS",
        "familia": "REDES LOCAIS PRINCIPAIS",
        "objeto": "TEXTO ASSOCIADO AO VÉRTICE DA REDE PRINCIPAL PLANIMÉTRICA *",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010202": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "ASSEMBLEIA MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010600": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010317": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESTAÇÃO EXPERIMENTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010112": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "PENITENCIÁRIA   ??????????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010904": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "ESQUADRA  (PSP)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080307": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO AÉREO",
        "objeto": "CONSTRUÇÕES ASSOCIADAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6160101": {
        "dominio": "Construções",
        "subdominio": null,
        "familia": "DETALHES",
        "objeto": "INDICAÇÃO DO SENTIDO DO ESCOAMENTO DAS ÁGUAS DE COBERTURA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120105": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "CURSOS DE ÁGUA",
        "objeto": "CANAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "88010104": {
        "tema": "ADICIONAIS",
        "camada": "ClassificaçõesAdicionais",
        "nome": "LimiteTrabalho",
        "map": [
            {
                "table": "area_trabalho",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    }
                ]
            }
        ]
    },
    "12010201": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIOS",
        "objeto": "RIO NAVEGÁVEL OU FLUTUÁVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090110": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "FEIRA POPULAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100501": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "ESTRADA FLORESTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9020401": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ZONAS VERDES",
        "familia": "PARQUES DE CAMPISMO",
        "objeto": "PARQUE DE CAMPISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4070504": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO",
        "familia": "DE RESÍDUOS",
        "objeto": "RESÍDUOS TÓXICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010207": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "JUNTA DE FREGUESIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020202": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ACUDES/REPRESAS",
        "objeto": "AÇUDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010601": {
        "tema": "NASCENTES E CURSOS DE ÁGUA",
        "camada": "NascentesCursosDeAgua_Rios_2D/3D",
        "nome": "LinhaDeAgua_3D",
        "map": [
            {
                "table": "curso_de_agua_eixo",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_curso_de_agua",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical",
                        "op": "set",
                        "value": 0
                    },
                    {
                        "src": "",
                        "dst": "delimitacao_conhecida",
                        "op": "set",
                        "value": true
                    },
                    {
                        "src": "",
                        "dst": "ficticio",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "6050111": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "AVIÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4020102": {
        "dominio": "Toponímia",
        "subdominio": "SERRAS",
        "familia": null,
        "objeto": "SERRA MÉDIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6160103": {
        "dominio": "Construções",
        "subdominio": null,
        "familia": "DETALHES",
        "objeto": "CÓDIGO ASSOCIADO AO Nº DE POLÍCIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010107": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DE ÁGUAS",
        "objeto": "ESTRUTURA DE CAPTAÇÃO DE ÁGUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010411": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "ENFERMARIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010208": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "ASSEMBLEIA REGIONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11020101": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "Mato",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": 5
                    }
                ]
            }
        ]
    },
    "6040203": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "OUTROS SERVIÇOS",
        "objeto": "GÁS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150203": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "SINALIZAÇÃO",
        "objeto": "SINAL DE TRÂNSITO VERTICAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010217": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "SEPARADOR / PROTECTOR ASSOCIADO IC/CIRC. REGIONAL EXTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010304": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "PISTA DE MOTOCICLISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2020103": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "LIMITES DO INE",
        "objeto": "LIMITE DE SECÇÃO ESTATÍSTICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13040001": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "CEMITÉRIOS",
        "familia": null,
        "objeto": "JAZIGO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FUNDAMENTAL",
        "objeto": "IP",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120502": {
        "tema": "INSTALAÇÕES DE RECREIO REPOUSO",
        "camada": "InstalacoesDeRecreioRepouso",
        "nome": "Estufa-Botânica",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "13"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "10110301": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "ESTRUTURAS ASSOCIADAS",
        "objeto": "PILAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061304": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "EDIFÍCIOS INDUSTRIAIS",
        "objeto": "MINA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010103": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "CIDADES",
        "objeto": "CAPITAL DE DISTRITO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010613": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "ANTA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12040602": {
        "dominio": "Hidrografia",
        "subdominio": "SUPERFÍCIES AQUÁTICAS",
        "familia": "PÂNTANOS",
        "objeto": "SAPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7000000": {
        "tema": "ÁREAS INDUSTRIAIS E DE SERVIÇOS",
        "camada": "AreasIndústriaisEdeServiços",
        "nome": "ÁreasIndustriaisEServiçosEmGeral",
        "map": [
            {
                "table": "areas_artificializadas",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_artificializadas",
                        "op": "set",
                        "value": 3
                    }
                ]
            }
        ]
    },
    "8010412": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "PROTEÇÃO DE PROJETOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010202": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": " MARCO GEODÉSICO DE 2ª E 3ª ORDEM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010706": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "SINAGOGA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "25"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.2"
                    }
                ]
            }
        ]
    },
    "6080201": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO FERROVIÁRIO",
        "objeto": "ESTAÇÃO DO CF",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090104": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA CADUCA",
        "objeto": "CARVALHOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120103": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "CURSOS DE ÁGUA",
        "objeto": "RIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010106": {
        "tema": "LIMITES ADMINISTRATIVOS FISCAIS E JURIDICOS",
        "camada": "Limites_LimitesAdministrativos",
        "nome": "LimiteDeConcelho",
        "map": [
            {
                "table": "concelho",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    }
                ]
            }
        ]
    },
    "6010805": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDIFÍCIOS SOCIAIS",
        "objeto": "CENTRO DE JUVENTUDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010500": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "CULTURA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120506": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "MARÉGRAFO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010508": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "TORRE/BARRA/POSTE DE MUITO ALTA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060401": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "HOSPITAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010302": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "ESTRADA MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010606": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "OUTROS",
        "objeto": "PASSADIÇO EM PRAIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010410": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "EXTENSÃO DE CENTRO DE SAÚDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061602": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "ESTAÇÃO DO CF",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010701": {
        "tema": "NASCENTES E CURSOS DE ÁGUA",
        "camada": "NascentesCursosDeAgua_Rios_2D/3D",
        "nome": "Vala",
        "map": [
            {
                "table": "curso_de_agua_eixo",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_curso_de_agua",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical",
                        "op": "set",
                        "value": 0
                    },
                    {
                        "src": "",
                        "dst": "delimitacao_conhecida",
                        "op": "set",
                        "value": false
                    },
                    {
                        "src": "",
                        "dst": "ficticio",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "8010705": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "ESTAÇÃO DE  EMISSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090107": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "CONDUTA ELEVADA (AQUEDUTO)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2040101": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "PORTÕES",
        "objeto": "PORTÃO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "8010101": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DE ÁGUAS",
        "objeto": "POÇO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020104": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA LARGA",
        "objeto": "VIA SIMPLES ELECTRIFICADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070201": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO FERROVIÁRIO  (C.  F. )",
        "objeto": "ESTAÇÃO           (PLATAFORMA)    ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030202": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "Socalco",
        "map": [
            {
                "table": "linha_de_quebra",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_classifica",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_natureza_linha",
                        "op": "set",
                        "value": 3
                    }
                ]
            }
        ]
    },
    "4060705": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "LOCAIS DE CULTO",
        "objeto": "SINAGOGA/MESQUITA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010309": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA DO ENSINO PROFISSIONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4130301": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS COM OUTRAS UTILIZAÇÕES",
        "familia": "ÁREAS  DE INTERESSE HISTÓRICO",
        "objeto": "MONUMENTOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010402": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE DE UTILIZAÇÃO LIMITADA",
        "objeto": "CAMINHO MILITAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010502": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "MUSEU",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050401": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "CENTRAIS DE ABASTECIMENTO",
        "objeto": "DEPÓSITO DE COMBUSTIVEL (PETRÓLEO E DERIVADOS LÍQUIDOS E LIQUEFEITOS)",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "26"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "7050601": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "RESPIRADOURO DE ATERRO SANITÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110103": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "PONTES PARA TRÁFEGO",
        "objeto": "PONTE DE MADEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10100103": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO TERRESTRE",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "ÁREAS DE SERVIÇO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010202": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "TAMPA DE CAIXA DE ESGOTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10090200": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS  DE APOIO AO TRÁFEGO MARÍTIMO|ÁREAS DE APOIO AO TRÁFEGO MARÍTIMO",
        "familia": "CAIS DE EMBARQUE",
        "objeto": "CAIS DE EMBARQUE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050207": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "TURISMO RURAL(Casa de Campo, Agro-Turismo, Hotel Rural)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010212": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DO IC/VIA RÁPIDA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010901": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "QUARTEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090103": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "CAMPO DE GOLFE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9020201": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ZONAS VERDES",
        "familia": "JARDINS ZOOLÓGICOS",
        "objeto": "JARDIM ZOOLÓGICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030114": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURO DE SUPORTE DE ENROCAMENTO (AE)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010204": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "SUMIDOURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020504": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "MEDIDORES DE NÍVEL",
        "objeto": "MARÉGRAFO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010506": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "EIXO DE ACEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010608": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "PALÁCIO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "21"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "10010305": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "VEREDA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4062103": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "objeto": "TERMAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090402": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "PETRÓLEO",
        "objeto": "ESTAÇÃO ASSOCIADA A OLEODUTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100303": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "CAMINHO MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090106": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "CROSS MOTORIZADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061403": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "PENSÃO/RESIDENCIAL/ALBERGARIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120507": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "PENÍNSULA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2020106": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "LIMITES DO INE",
        "objeto": "CÓDIGO DE SUB-SECÇÃO ESTATÍSTICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080286": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO FERROVIÁRIO",
        "objeto": "APEADEIRO sem representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080402": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO MARÍTIMO",
        "objeto": "DOCA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050303": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS ARTESANAIS/OFICINAS",
        "objeto": "OFICINAS DE REPARAÇÃO AUTOMÓVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080309": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO AÉREO",
        "objeto": "TERMINAL DE AEROPORTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13080100": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": null,
        "familia": null,
        "objeto": "ÁREAS ALTERADAS/EM ALTERAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070300": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO FERROVIÁRIO  ( METRO )",
        "objeto": "TRÁFEGO FERROVIÁRIO (METRO)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100205": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEME NTAR",
        "objeto": "IC / CIRCULAR REGIONAL EXTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050209": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "CONJUNTOS TURÍSTICOS (RESORTS)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010308": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "ESCADARIAS",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "9010600": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISCINAS",
        "objeto": "PISCINAS EM GERAL",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 3
                    }
                ]
            }
        ]
    },
    "10010713": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "EIXO DA PISTA DE MOTOCICLISMO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 8
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "10010303": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "CAMINHO MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9020102": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ZONAS VERDES",
        "familia": "PARQUES E JARDINS",
        "objeto": "BANCO DE JARDIM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010619": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "LUNETA/MONÓCULO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010305": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA BÁSICA DO 1º CICLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010102": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DE ÁGUAS",
        "objeto": "FURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050105": {
        "tema": "EDIFICIOS INDUSTRIAIS",
        "camada": "EdificiosIndustriais",
        "nome": "Fábrica",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_condicao_const",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": 14
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": 27
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": 3
                    }
                ]
            }
        ]
    },
    "4060403": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "MATERNIDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010602": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "PADRÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11110101": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS DE EXPLORAÇÃO PECUÁRIA",
        "familia": "AQUACULTURA",
        "objeto": "VIVEIROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050104": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "ESTALEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010323": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "RESIDÊNCIA UNIVERSITÁRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010102": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "TRIBUNAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040205": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "OUTROS SERVIÇOS",
        "objeto": "ÁGUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6000000": {
        "tema": "CONTRUÇÕES",
        "camada": "Construções",
        "nome": "ConstruçõesEmGeral",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": "todo"
                    }
                ]
            }
        ]
    },
    "6010322": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "BANDA FILARMÓNICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060504": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "CULTURA",
        "objeto": "MUSEU",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010303": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "CANAIS",
        "objeto": "CANAIS SUPERFÍCIAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020110": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "PISO RECUADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010209": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "REPARTIÇÃO DE FINAÇAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060606": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "MONUMENTOS",
        "objeto": "RUÍNAS C/INTER. HISTÓRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090109": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "CAMPO DE TÉNIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070202": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO FERROVIÁRIO  (C.  F. )",
        "objeto": "APEADEIRO  (PLATAFORMA)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010309": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "RADIO MODELISMO AUTOMÓVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090105": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "DEPÓSITO DE ÁGUA SUBTERRÂNEO S/ representação à escala",
        "map": [
            {
                "table": "elem_assoc_agua",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_associado_agua",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "12010605": {
        "tema": "NASCENTES E CURSOS DE ÁGUA",
        "camada": "NascentesCursosDeAgua_Rios_2D/3D",
        "nome": "Linha_Água_Encoberta",
        "map": [
            {
                "table": "curso_de_agua_eixo",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_curso_de_agua",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical",
                        "op": "set",
                        "value": 0
                    },
                    {
                        "src": "",
                        "dst": "delimitacao_conhecida",
                        "op": "set",
                        "value": false
                    },
                    {
                        "src": "",
                        "dst": "ficticio",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "12010702": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "VALAS",
        "objeto": "VALETA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150302": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "ÁREAS COMERCIAIS",
        "objeto": "QUIOSQUE FIXO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10080302": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO ÁEREO",
        "familia": "HELIPORTOS",
        "objeto": "HELIPORTO SOBRE EDIFÍCIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120301": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "CAMPISMO",
        "objeto": "EDIFÍCIOS DO PARQUE DE CAMPISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070301": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO FERROVIÁRIO  ( METRO )",
        "objeto": "ESTAÇÃO SUBTERRÂNEA      (PLATAFORMA)   ??????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11100102": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "AreaAgricolaFlorestalGeral",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": 3
                    }
                ]
            }
        ]
    },
    "1030104": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDE FOTOGRAMÉTRICA",
        "familia": "PONTOS FOTOGRAMÉTRICOS",
        "objeto": "TEXTO ASSOCIADO A UM PONTO FOTOGRAMÉTRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120601": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "EXCURSIONISMO/PEDESTRIANISMO",
        "objeto": "MIRADOURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150402": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "RECOLHA DE RESÍDUOS SÓLIDOS",
        "objeto": "ECOPONTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010501": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "AQUEDUTOS",
        "objeto": "AQUEDUTO SUPERFICIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120510": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "ÍNSUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010103": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "CurvaDeNívelSecundária_3D",
        "map": [
            {
                "table": "curva_de_nivel",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_curva",
                        "op": "set",
                        "value": 2
                    }
                ]
            }
        ]
    },
    "6050110": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "SUINICULTURA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010901": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA SOLAR",
        "objeto": "PAINEL SOLAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120201": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "TERMAS",
        "objeto": "EDIFÍCIOS DAS TERMAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2020102": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "LIMITES DO INE",
        "objeto": "CÓDIGO DE NUT",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010308": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA DO ENSINO ESPECIAL  (DEFICIENTES)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7050200": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "ÁREA PARA TRATAMENTO DE RESÍDUOS LÍQUIDOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010231": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DA ESTRADA REGIONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010504": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "POSTE DE ILUMINAÇÃO MÚLTIPLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120104": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "CURSOS DE ÁGUA",
        "objeto": "RIBEIRO / RIBEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050801": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "FABRICO E ARMAZENAMENTO DE MATERIAIS EXPLOSIVOS",
        "objeto": "FABRICA DE MATERIAIS EXPLOSIVOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010204": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "TEXTO ASSOCIADO AO PONTO MAIS ELEVADO DAS CONSTRUÇÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010607": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "CASTELO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "8"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.3"
                    }
                ]
            }
        ]
    },
    "6090114": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "VÁLVULA DE CORTE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050601": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "TRANSPORTE",
        "objeto": "CAIS (EMBARQUE/DESEMBARQUE)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060901": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "FORTE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010610": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "CRUZEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7010000": {
        "dominio": "Indústrias",
        "subdominio": "EXPLORAÇÕES MINEIRAS",
        "familia": null,
        "objeto": "EXPLORAÇÕES MINEIRAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090205": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA PERSISTENTE",
        "objeto": "MONTADO (  SOBRO  +  AZINHO )",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010218": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DO IC/RADIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010310": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "EIXO DA ESTRADA MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010001": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "LANCIS",
        "objeto": "LANCIL",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 9
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "10110105": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "PONTES PARA TRÁFEGO",
        "objeto": "PONTE LEVADIÇA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1070101": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDES LOCAIS",
        "familia": "REDES LOCAIS PRINCIPAIS",
        "objeto": "VÉRTICE DE REDE PRINCIPAL PLANIMÉTRICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060703": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "LOCAIS DE CULTO",
        "objeto": "BASÍLICA, CATEDRAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050402": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "CENTRAIS DE ABASTECIMENTO",
        "objeto": "DEPÓSITO DE GÁS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080400": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO MARÍTIMO",
        "objeto": "TRÁFEGO MARÍTIMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010700": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "LOCAIS DE CULTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010220": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DA EN",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030105": {
        "tema": "OUTROS LIMITES",
        "camada": "OutrosLimites_Muros",
        "nome": "MuroDeSuporteDePedraSolta",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "8010605": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "GÁS, PETRÓLEO E PRODUTOS QUÍMICOS",
        "objeto": "CAIXA DE VISITA DE GÁS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020404": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ESTRUTURAS ASSOCIADAS E OUTRAS",
        "objeto": "ECLUSA                                    ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13030300": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "ÁREAS  DE INTERESSE HISTÓRICO",
        "familia": null,
        "objeto": "ESTAÇÕES ARQUEOLÓGICAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010301": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "CANAIS",
        "objeto": "MINAS DE ÁGUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090103": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "DEPÓSITO DE ÁGUA ELEVADO (sem representação à escala)",
        "map": [
            {
                "table": "elem_assoc_agua",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_associado_agua",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "4090204": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "PARQUES E JARDINS",
        "objeto": "PARQUE DE CAMPISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120203": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "MANCHAS DE ÁGUA",
        "objeto": "BARRAGEM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030203": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "COMBRO (Limite Superior)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010312": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA SECUNDÁRIA TECNOLÓGIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090110": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "BOCA DE REGA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050112": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "BALANÇA PARA ANIMAIS VIVOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010209": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM FORTE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010204": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "TRIBUNAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060501": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "CULTURA",
        "objeto": "PLANETÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030208": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "ATERRO (Limite Inferior)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010212": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM POSTO DE VIGIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090106": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "CHAFARIZ, BICA, FONTANÁRIO, FONTE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010403": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "CENTRO DE SAÚDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060103": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "PRISÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010408": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "CLÍNICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13040000": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "CEMITÉRIOS",
        "familia": null,
        "objeto": "CEMITÉRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010400": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "SAÚDE/HIGIENE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010215": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "SEPARADOR / PROTECTOR ASSOCIADO AO IC/CIRC. REGIONAL INTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120401": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "ZONAS INUNDÁVEIS",
        "objeto": "SAPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010409": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "PROJETOR ELÉTRICO NO SOLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10120102": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "PASSAGENS NIVELADAS, DESNIVELADAS E TÚNEIS",
        "objeto": "PASSAGEM INFERIOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010806": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDIFÍCIOS SOCIAIS",
        "objeto": "ASSOCIAÇÕES SOCIAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010200": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "ADMINISTRAÇÃO REGIONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010400": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "REGADEIRAS   ??????????????????????????????????????",
        "objeto": "REGADEIRA OU REGUEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030209": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "DESATERRO (Limite Superior)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100301": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "EN",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2020105": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "LIMITES DO INE",
        "objeto": "LIMITE DE SUB-SECÇÃO ESTATÍSTICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150401": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "RECOLHA DE RESÍDUOS SÓLIDOS",
        "objeto": "CONTENTORES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030307": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "VEDAÇÕES",
        "objeto": "PILAR DE GUARDA CORPOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010302": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ATLs - ATIVIDADES DE TEMPOS LIVRES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080107": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "ÁREAS DE SERVIÇO(restaurante,estação de serviço,etc) ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010604": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "OUTROS",
        "objeto": "EIXO DO CAMINHO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030302": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "VEDAÇÕES",
        "objeto": "GRADEAMENTO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "4061404": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "MOTEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010103": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "CAMPOS DE JOGOS",
        "objeto": "LIMITE DO RECINTO DE PRÁTICA DESPORTIVA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6030100": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS COMERCIAIS",
        "familia": "GRANDES DIMENSÕES",
        "objeto": "GRANDES DIMENSÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020200": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ACUDES/REPRESAS",
        "objeto": "AÇUDES/REPRESAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010606": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIBEIRAS /LINHAS DE ÁGUA",
        "objeto": "RIBEIRA/RIBEIRO COBERTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061603": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "APEADEIRO DO CF",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050200": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "INDÚSTRIAS HOTELEIRAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4170101": {
        "dominio": "Toponímia",
        "subdominio": "MOLDURA",
        "familia": null,
        "objeto": "ELEMENTOS BÁSICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090303": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "GÁS",
        "objeto": "ESTAÇÃO ASSOCIADA A GASODUTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1070105": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDES LOCAIS",
        "familia": "REDES LOCAIS PRINCIPAIS",
        "objeto": "VÉRTICE DE TRIANGULAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1070107": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDES LOCAIS",
        "familia": "REDES LOCAIS PRINCIPAIS",
        "objeto": "MARCA DE NIVELAMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100305": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "VEREDA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061401": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "HOTEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6100000": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "INSTALAÇÕES DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3020202": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "LimiteInferiorDeEscarpado(Natural)",
        "map": [
            {
                "table": "linha_de_quebra",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_classifica",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_natureza_linha",
                        "op": "set",
                        "value": 3
                    },
                    {
                        "src": "",
                        "dst": "artificial",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "6070201": {
        "tema": "EDIFICIOS COM OUTRAS UTILIZAÇÕES",
        "camada": "EdifíciosComOutrasUtilizações_MoinhosNaoIndustriais",
        "nome": "EdifícioEmRuínas",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_condicao_const",
                        "op": "set",
                        "value": 5
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": 14
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": 27
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "todo"
                    }
                ]
            }
        ]
    },
    "6010118": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "BANCO DE PORTUGAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090202": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ELECTRICIDADE ",
        "objeto": "POSTO DE TRANSFORMAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010206": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "IC / RADIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050502": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "ARMAZENAGEM",
        "objeto": "SILO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "24"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "3"
                    }
                ]
            }
        ]
    },
    "10010407": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE DE UTILIZAÇÃO LIMITADA",
        "objeto": "EIXO DA ESTRADA PARTICULAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060105": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "POSTO FRONTEIRIÇO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090116": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "CALEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10100101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO TERRESTRE",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "TERMINAL TIR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060506": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "CULTURA",
        "objeto": "CINEMA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010303": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "MARCOS ADIMINISTRATIVOS",
        "objeto": "TEXTO ASSOCIADO AO MARCO DE FRONTEIRA *",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080106": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARQUE DE ESTACIONAMENTO EM SILO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010708": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "CAIXA DE VISITA DE TELECOMUNICAÇÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030107": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURO COM GRADEAMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12040301": {
        "tema": "NASCENTES E CURSOS DE ÁGUA",
        "camada": "NascentesCursosDeAgua_Rios_2D/3D",
        "nome": "Lagoa_3D",
        "map": [
            {
                "table": "agua_lentica",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_agua_lentica",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "cota_pleno_armazenamento",
                        "op": "set",
                        "value": false
                    },
                    {
                        "src": "",
                        "dst": "mare",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "4060507": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "CULTURA",
        "objeto": "BIBLIOTECA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6100100": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "INSTALAÇÃO PARA TRATAMENTO DE RESÍDUOS SÓLIDOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11040100": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "Vinha",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "1.3"
                    }
                ]
            }
        ]
    },
    "11010102": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "Sequeiro",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "1.1"
                    }
                ]
            }
        ]
    },
    "10070101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "CENTRAL DE CAMIONAGEM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010304": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "CAMINHO VICINAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061608": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "INSTITUTO DE SOCORROS A NÁUFRAGOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010209": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "CAIXA DE VISITA DE RESÍDUOS DOMÉSTICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010105": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "NASCENTES",
        "objeto": "MÃE DE ÁGUA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "18"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "10100105": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO TERRESTRE",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "LUGAR DE ESTACIONAMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010102": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "CURVAS DE NÍVEL",
        "objeto": "ÍNDICE DA CURVA MESTRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010215": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "TEXTO ASSOCIADO AO PONTO DE COTA EM TABULEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010208": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM CASTELO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010403": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE DE UTILIZAÇÃO LIMITADA",
        "objeto": "ESTRADA PARTICULAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010211": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM FAROL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020108": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "PILAR CIRCULAR DE CASA ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020103": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "COMPORTAS",
        "objeto": "COMPORTA sem representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13080101": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": null,
        "familia": null,
        "objeto": "NÚCLEO DEGRADADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090101": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA CADUCA",
        "objeto": "CASTANHEIROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090101": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "ÁREA DESPORTIVA ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010320": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESTABELECIMENTO DE ENSINO PRIVADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120401": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "ZOOLOGIA",
        "objeto": "EDIFÍCIOS ASSOCIADOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7010401": {
        "dominio": "Indústrias",
        "subdominio": "EXPLORAÇÕES MINEIRAS",
        "familia": null,
        "objeto": "MINA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040204": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "OUTROS SERVIÇOS",
        "objeto": "ELECTRICIDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090301": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "Mata",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": 4
                    }
                ]
            }
        ]
    },
    "6080285": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO FERROVIÁRIO",
        "objeto": "CONSTRUÇÕES ASSOCIADAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030100": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MUROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060502": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "CULTURA",
        "objeto": "OBSERVATÓRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010702": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "HIPÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030303": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "VEDAÇÕES",
        "objeto": "VEDAÇÃO DE MADEIRA",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "10010405": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE DE UTILIZAÇÃO LIMITADA",
        "objeto": "EIXO DA ESTRADA  MILITAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090302": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "GÁS",
        "objeto": "GASÓMETRO  ?????????????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080111": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARQUÍMETRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061605": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "DOCA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11020102": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS  VERDES (NÃO ARÁVEIS)",
        "familia": "ÁREAS VERDES (NÃO ARÁVEIS)",
        "objeto": "CANAVIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010203": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "PONTO MAIS ELEVADO DAS CONSTRUÇÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010403": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "CABO DE TRANSPORTE AÉREO DE MÉDIA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010303": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "JARDIM-DE-INFÂNCIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4070503": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO",
        "familia": "DE RESÍDUOS",
        "objeto": "RESÍDUOS INDUSTRIAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12050103": {
        "dominio": "Hidrografia",
        "subdominio": "HIDROGRAFIA COSTEIRA",
        "familia": "MARGENS CONSOLIDADAS, LINHAS DE COSTA",
        "objeto": "MARGEM COM ESPORÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010406": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "CABO DE TRANSPORTE DE MUITO ALTA TENSÃO SUBTERRÂNEO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1070102": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDES LOCAIS",
        "familia": "REDES LOCAIS PRINCIPAIS",
        "objeto": "VÉRTICE DE REDE PRINCIPAL ALTIMÉTRICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10120107": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "PASSAGENS NIVELADAS, DESNIVELADAS E TÚNEIS",
        "objeto": "BOCA DE TÚNEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010109": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "LIMITES ADMINISTRATIVOS",
        "objeto": "LIMITE NÃO DEFINIDO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010201": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "CAMPOS DE GOLFE",
        "objeto": "CAMPO DE GOLFE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010301": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "CRECHE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12050105": {
        "dominio": "Hidrografia",
        "subdominio": "HIDROGRAFIA COSTEIRA",
        "familia": "MARGENS CONSOLIDADAS, LINHAS DE COSTA",
        "objeto": "MARGEM FIRME/ROCHOSA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060604": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "MONUMENTOS",
        "objeto": "CASTELO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020402": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ESTRUTURAS ASSOCIADAS E OUTRAS",
        "objeto": "DESCARREGADOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010510": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "COLISEU",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050100": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "PRODUÇÃO/TRANSFORMAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010501": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "POSTE DE ALTA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010513": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "IMÓVEL EM VIAS DE CLASSIFICAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050203": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "EDIFÍCIO DE ALDEAMENTO TURÍSTICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010301": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "MARCOS ADIMINISTRATIVOS",
        "objeto": "MARCO DE FRONTEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010203": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM IGREJA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050301": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS ARTESANAIS/OFICINAS",
        "objeto": "OFICINAS EM GERAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120202": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "MANCHAS DE ÁGUA",
        "objeto": "ALBUFEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060503": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "CULTURA",
        "objeto": "CENTRO CULTURAL ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010603": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "GÁS, PETRÓLEO E PRODUTOS QUÍMICOS",
        "objeto": "OUTROS PRODUTOS  ?????????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010300": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "CANAIS",
        "objeto": "CANAIS EM GERAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010207": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "CRUZEIRO SERVINDO DE GEODÉSICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4130201": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS COM OUTRAS UTILIZAÇÕES",
        "familia": "ÁREAS PROTEGIDAS",
        "objeto": "PARQUES / RESERVAS NATURAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090205": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ELECTRICIDADE ",
        "objeto": "INSTALAÇÕES ASSOCIADAS A GERADORES EÓLICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020401": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ESTRUTURAS ASSOCIADAS E OUTRAS",
        "objeto": "PAREDÃO DE BARRAGEM",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "5"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "6120107": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "INSTALAÇÕES DESPORTIVAS",
        "objeto": "ESTÁDIO    ???????????????                                                 ANULAR",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "12"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.4"
                    }
                ]
            }
        ]
    },
    "10050103": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "OUTROS   ????????????????????????????",
        "objeto": "PASSEIO FLUVIAL/MARÍTIMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010405": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "CABO DE TRANSPORTE AÉREO DE MUITO ALTA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010801": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "MARCAÇÕES",
        "objeto": "MARCO QUILOMÉTRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020111": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "FRENTES ELEVADAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010115": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "ALFÂNDEGA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090102": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "CENTRAL ELEVATÓRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090109": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "LINHA ASSOCIADA AO CHAFARIZ, FONTANÁRIO OU FONTE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010807": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDIFÍCIOS SOCIAIS",
        "objeto": "LAR DE INFÂNCIA /JUVENTUDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090112": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "PORTINHOLA DE ÁGUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010302": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DA ENERGIA EÓLICA|APROVEITAMENTO DE ENERGIA EÓLICA",
        "objeto": "GERADOR EÓLICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080301": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO AÉREO",
        "objeto": "HANGAR",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "16"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "6.6"
                    }
                ]
            }
        ]
    },
    "8010505": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "TORRE DE ILUMINAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2050000": {
        "dominio": "Limites",
        "subdominio": "ÁREAS EM CONTRUÇÃO/ALTERAÇÃO",
        "familia": null,
        "objeto": "LIMITE DE ÁREA EM CONSTRUÇÃO/ALTERAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010404": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "CAIXA DE VISITA DE ELETRICIDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010318": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "RAMPA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030305": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "VEDAÇÕES",
        "objeto": "VEDAÇÃO DE REDE (AE)",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "3010216": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "PONTO DE COTA EM SOLEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090107": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "PRAÇA DE TOUROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010701": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "TRÁFEGO AÉREO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010111": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "ESTABELECIMENTO PRISIONAL/PRISÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010210": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "PONTO DE COTA EM CUMEEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010201": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "VILAS",
        "objeto": "SEDE DE CONCELHO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010708": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "CAPELA sem representação à escala  ????????????",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "7"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.2"
                    }
                ]
            }
        ]
    },
    "6010311": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "UNIVERSIDADE/FACULDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061601": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "ÁREAS DE SERVIÇO (RESTAURANTE, ESTAÇÃO DE SERVIÇO, ETC)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010410": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "PROJETOR ELÉTRICO ELEVADO DO SOLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010900": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "VIAS RODOVIÁRIAS EM CONSTRUÇÃO",
        "objeto": "VIAS RODOVIÁRIA EM CONSTRUÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060902": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "ESQUADRA PSP",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080105": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "ESTAÇÃO, TERMINAL (DE CAMIONAGEM)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060201": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "CÂMARA MUNICIPAL, SECRETARIA REGIONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090203": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ELECTRICIDADE ",
        "objeto": "SUBESTAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010508": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "TEATRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120102": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "CURSOS DE ÁGUA",
        "objeto": "RIO SECUNDÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090401": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "PETRÓLEO",
        "objeto": "DEPÓSITO DE PETRÓLEO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "26"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "6010318": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA SECUNDÁRIA ARTÍSTICA/CONSERVATÓRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010300": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "EDUCAÇÃO/INVESTIGAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010202": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "IC / AE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010511": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "CENTRO CULTURAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010204": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM CAPELA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11030000": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS  HORTO-FRUTÍCOLAS",
        "familia": null,
        "objeto": "ÁREAS HORTO-FRUTÍCULAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010411": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "PROJETOR ELÉTRICO DE PÉ ALTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6100200": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "INSTALAÇÃO PARA TRATAMENTO DE RESÍDUOS LÍQUIDOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010507": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "TORRE DE ALTA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050503": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "ARMAZENAGEM",
        "objeto": "SILO  sem representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010706": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "ESTAÇÃO DE TELECOMUNICAÇÕES   ??????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090104": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "HIPÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060701": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "LOCAIS DE CULTO",
        "objeto": "CAPELA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050205": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "POUSADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060603": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "MONUMENTOS",
        "objeto": "FORTE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12060000": {
        "dominio": "Hidrografia",
        "subdominio": "ELEMENTOS ESPECIAIS",
        "familia": null,
        "objeto": "LINHA DE REGOLFO DAS ALBUFEIRAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12030101": {
        "dominio": "Hidrografia",
        "subdominio": "PORTOS",
        "familia": null,
        "objeto": "PORTO FLUVIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080300": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO AÉREO",
        "objeto": "TRÁFEGO AÉREO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7050400": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "ÁREA PARA TRATAMENTO DE RESÍDUOS TÓXICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050204": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "EDIFÍCIO DE APARTAMENTOS TURÍSTICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010104": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "CAMPOS DE JOGOS",
        "objeto": "MARCAÇÕES DOS CAMPOS DE JOGOS",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 5
                    }
                ]
            }
        ]
    },
    "9010501": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "CARREIRAS DE TIRO",
        "objeto": "CAMPO  DE TIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070104": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARAGEM DE TÁXIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010504": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "BIBLIOTECA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010101": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "CAMPOS DE JOGOS",
        "objeto": "CAMPO DE JOGOS COM BANCADAS",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 5
                    }
                ]
            }
        ]
    },
    "11090102": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA CADUCA",
        "objeto": "CHOUPOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030101": {
        "tema": "OUTROS LIMITES",
        "camada": "OutrosLimites_Muros",
        "nome": "MuroDeAlvernaria",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "12030201": {
        "dominio": "Hidrografia",
        "subdominio": "PORTOS",
        "familia": null,
        "objeto": "PORTO MARÍTIMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090206": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "Sobreiros",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "4.1.1"
                    }
                ]
            }
        ]
    },
    "10120103": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "PASSAGENS NIVELADAS, DESNIVELADAS E TÚNEIS",
        "objeto": "PASSAGEM SUPERIOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010107": {
        "tema": "LIMITES ADMINISTRATIVOS FISCAIS E JURIDICOS",
        "camada": "Limites_LimitesAdministrativos",
        "nome": "LimiteDeFreguesia",
        "map": [
            {
                "table": "freguesia",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    }
                ]
            }
        ]
    },
    "4090203": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "PARQUES E JARDINS",
        "objeto": "JARDIM BOTÂNICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060301": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESTAÇÃO EXPERIMENTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6130100": {
        "dominio": "Construções",
        "subdominio": "REPRESENTAÇÕES DIPLOMÁTICAS",
        "familia": null,
        "objeto": "EMBAIXADA,CONSULADO,MISSÃO, ETC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6030103": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS COMERCIAIS",
        "familia": "GRANDES DIMENSÕES",
        "objeto": "MERCADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11010000": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS  ARÁVEIS",
        "familia": null,
        "objeto": "ÁREAS ARÁVEIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110201": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "OUTRAS PONTES",
        "objeto": "PONTE PARA PEÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020603": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "TANQUES/LAGOS PEQUENOS",
        "objeto": "TANQUE  (VIVEIRO)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010215": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "SINAL GEODÉSICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120108": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "INSTALAÇÕES DESPORTIVAS",
        "objeto": "BALNEÁRIOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4170201": {
        "dominio": "Toponímia",
        "subdominio": "MOLDURA",
        "familia": null,
        "objeto": "OUTROS ELEMENTOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030101": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "LimiteSuperiorDeEscarpado(Artificial)",
        "map": [
            {
                "table": "linha_de_quebra",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_classifica",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_natureza_linha",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "artificial",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "11090103": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA CADUCA",
        "objeto": "SALGUEIROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10120101": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "PASSAGENS NIVELADAS, DESNIVELADAS E TÚNEIS",
        "objeto": "PASSAGEM DE NÍVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010702": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "CAPELA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "7"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.2"
                    }
                ]
            }
        ]
    },
    "1010210": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM DEPÓSITO DE ÁGUA ELEVADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010701": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "POSTE TELEFÓNICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120500": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "BOTÂNICA",
        "objeto": "BOTÂNICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010909": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "POLÍCIA JUDICIÁRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10040101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ASCENSORES/ELEVADORES",
        "familia": "ELEVADORES",
        "objeto": "ELEVADOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010605": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "OUTROS",
        "objeto": "OUTROS SEPARADORES/PROTETORES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110204": {
        "tema": "OBRAS DE ARTE - PONTES",
        "camada": "ObrasArte_PontesParaTrafego",
        "nome": "Aqueduto",
        "map": [
            {
                "table": "obra_arte",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_obra_arte",
                        "op": "set",
                        "value": 6
                    }
                ]
            }
        ]
    },
    "9010801": {
        "tema": "ÁREAS DA LAZER E RECREIO",
        "camada": "AreasLazerRecreio_AreasDesportivas",
        "nome": "CampoDeTénis",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 5
                    }
                ]
            }
        ]
    },
    "4100202": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEME NTAR",
        "objeto": "IC / AE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110104": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "PONTES PARA TRÁFEGO",
        "objeto": "PONTE GIRATÓRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010617": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "MENIR Sem representação à escala  ???????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010406": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "CENTRO DE RECUPERAÇÃO E REABILITAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020106": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "PAREDES  RECUADAS/CORPO RECUADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010101": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "CurvaDeNívelMestra_3D",
        "map": [
            {
                "table": "curva_de_nivel",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_curva",
                        "op": "set",
                        "value": 1
                    }
                ]
            }
        ]
    },
    "8010509": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "TORRE DE MÉDIA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3020101": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM NATURAL)",
        "familia": " ROCHAS E AREIAS",
        "objeto": "ROCHAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100207": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEME NTAR",
        "objeto": "EN",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010512": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "CORETO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "10"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.3"
                    }
                ]
            }
        ]
    },
    "4150106": {
        "dominio": "Toponímia",
        "subdominio": null,
        "familia": null,
        "objeto": "NÚMERO DE POLÍCIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040202": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "OUTROS SERVIÇOS",
        "objeto": "SEGUROS/Agência de seguros",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010908": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "EDIFICIO DA GNR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010605": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "ESTÁTUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040103": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "COMUNICAÇÕES",
        "objeto": "COMUNICAÇÃO SOCIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010604": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIBEIRAS /LINHAS DE ÁGUA",
        "objeto": "EIXO DA RIBEIRA/RIBEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010607": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIBEIRAS /LINHAS DE ÁGUA",
        "objeto": "RIO COBERTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100203": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEME NTAR",
        "objeto": "IC / VIA RÁPIDA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010212": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "PONTO DE COTA NO PAVIMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010208": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "PONTO DE COTA EM POLÍGONO NO TOPO DO EDIFÍCIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100503": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "CAMINHO VICINAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4062101": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "objeto": "PAVILHÃO GIMNO-DESPORTIVO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7050100": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "ÁREA PARA TRATAMENTO DE RESÍDUOS SÓLIDOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010902": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PARQUES DE DIVERSÃO",
        "objeto": "PARQUE AQUÁTICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010207": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EN",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010302": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "MARCOS ADIMINISTRATIVOS",
        "objeto": "MARCO DE CONCELHO / FREGUESIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010503": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "ACEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150102": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "HIGIENE",
        "objeto": "SANITÁRIOS PÚBLICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060102": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "TRIBUNAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150101": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "HIGIENE",
        "objeto": "LAVADOUROS PÚBLICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010602": {
        "tema": "VIAS COMUNICAÇÃO - TRÁFEGO RODOVIÁRIO",
        "camada": "TráfegoRodoviário",
        "nome": "Caminho",
        "map": [
            {
                "table": "seg_via_rodov",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_estado_via_rodov",
                        "op": "set",
                        "value": 5
                    },
                    {
                        "src": "",
                        "dst": "valor_caract_fisica_via_rodov",
                        "op": "set",
                        "value": 3
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical_transportes",
                        "op": "set",
                        "value": 0
                    },
                    {
                        "src": "",
                        "dst": "valor_sentido",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_circulacao",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_troco_rodoviario",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "num_vias_transito",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "pavimentado",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "8010604": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "GÁS, PETRÓLEO E PRODUTOS QUÍMICOS",
        "objeto": "PARQUE DE ARMAZENAGEM DE GARRAFAS DE GÁS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080403": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO MARÍTIMO",
        "objeto": "EDIFÍCIO DO ISN",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4101001": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "LOCAIS DE PARAGEM - ÁREAS DE APOIO AO TRÁFEGO TERRESTRE",
        "objeto": "TERMINAL TIR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010601": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "GÁS, PETRÓLEO E PRODUTOS QUÍMICOS",
        "objeto": "GASODUTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010316": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "CÓDIGO ASSOCIADO AO NOME DE RUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050702": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "MATÉRIAS EXTRATIVAS",
        "objeto": "SALINA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6030102": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS COMERCIAIS",
        "familia": "GRANDES DIMENSÕES",
        "objeto": "CENTRO COMERCIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7090100": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DE DEPÓSITO DE LIXOS",
        "familia": null,
        "objeto": "VAZADOURO/LIXEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010105": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "CIDADES",
        "objeto": "CIDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4070502": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO",
        "familia": "DE RESÍDUOS",
        "objeto": "RESÍDUOS LÍQUIDOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010102": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FUNDAMENTAL",
        "objeto": "IP / AE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12050101": {
        "dominio": "Hidrografia",
        "subdominio": "HIDROGRAFIA COSTEIRA",
        "familia": "MARGENS CONSOLIDADAS, LINHAS DE COSTA",
        "objeto": "VERTICAL (PAREDÃO, MURO CAIS)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090113": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "CAIXA DE VÁLVULA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010706": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "EIXO DO HIPÓDROMO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 8
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "4130302": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS COM OUTRAS UTILIZAÇÕES",
        "familia": "ÁREAS  DE INTERESSE HISTÓRICO",
        "objeto": "RUÍNAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2020101": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "LIMITES DO INE",
        "objeto": "LIMITE DE NUT",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13020200": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "ÁREAS PROTEGIDAS",
        "familia": "PARQUES E RESERVAS NATURAIS",
        "objeto": "PARQUE / RESERVA NATURAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10050101": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "OUTROS   ????????????????????????????",
        "objeto": "LINHA DE TELEFÉRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010206": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "PONTO DE COTA NO BEIRAL DO TELHADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020107": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "PILAR DE CASA (Dimensão significativa)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050201": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "HOTEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080109": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "BOMBA GASOLINA (só as isoladas)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030201": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "AterroDesaterro",
        "map": [
            {
                "table": "linha_de_quebra",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_classifica",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "valor_natureza_linha",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "artificial",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "8010105": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DE ÁGUAS",
        "objeto": "NORA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3020102": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM NATURAL)",
        "familia": " ROCHAS E AREIAS",
        "objeto": "DUNAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010100": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "ADMINISTRAÇÃO CENTRAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090112": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "KARTÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100306": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "RUA, ETC, ...",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7050000": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010201": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "PontoCotado_3D",
        "map": [
            {
                "table": "ponto_cotado",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "point_f"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_classifica_las",
                        "op": "set",
                        "value": 1
                    }
                ]
            }
        ]
    },
    "10010213": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "SEPARADOR / PROTECTOR ASSOCIADO IC/VIA RÁPIDA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070100": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "TRÁFEGO RODOVIÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061607": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "CENTRAL DE CAMIONAGEM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6030104": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS COMERCIAIS",
        "familia": "GRANDES DIMENSÕES",
        "objeto": "SUPERMERCADO/HIPERMERCADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010714": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "EIXO DO VELÓDROMO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 8
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "6010618": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "ANTA Sem representação à escala ????????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10120105": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "PASSAGENS NIVELADAS, DESNIVELADAS E TÚNEIS",
        "objeto": "PASSADEIRA DE PEÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040201": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "OUTROS SERVIÇOS",
        "objeto": "BANCOS/Agência Bancária",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020208": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "SOLEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3020103": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM NATURAL)",
        "familia": " ROCHAS E AREIAS",
        "objeto": "AREIAS    ?????????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080284": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO FERROVIÁRIO",
        "objeto": "POSTE DE LINHA DE TELEFÉRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030113": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURO DE SUPORTE DE BETÃO ARMADO (AE)",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "6090108": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "CONDUTA SUBTERRÂNEA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010602": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "GÁS, PETRÓLEO E PRODUTOS QUÍMICOS",
        "objeto": "OLEODUTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010302": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "AUTÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100206": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEME NTAR",
        "objeto": "IC / RADIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010707": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "EIXO DA PISTA DO AUTÓDROMO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 8
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "8010206": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "CAIXA DE VISITA PLUVIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010304": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "MARCOS ADIMINISTRATIVOS",
        "objeto": "TEXTO ASSOCIADO AO MARCO DE CONC/FREG *",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010712": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "EIXO DO KARTÓDROMO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 8
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "10070102": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARAGEM DE AUTOCARRO - ABRIGO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120702": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "OUTRAS INSTALAÇÕES",
        "objeto": "POUSADA DA JUVENTUDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4062102": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "objeto": "ESTÁDIO, TRIBUNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090117": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "DEPÓSITO DE ÁGUA À SUPERFÍCIE sem representação à escala",
        "map": [
            {
                "table": "elem_assoc_agua",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_associado_agua",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "12010503": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "AQUEDUTOS",
        "objeto": "AQUEDUTO SOBRE ARCADAS OU PILARES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1070106": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDES LOCAIS",
        "familia": "REDES LOCAIS PRINCIPAIS",
        "objeto": "VÉRTICE DE POLIGONAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010307": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA BÁSICA DO 2º CICLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110106": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "PONTES PARA TRÁFEGO",
        "objeto": "PONTE PÊNSIL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010312": {
        "tema": "VIAS COMUNICAÇÃO - TRÁFEGO RODOVIÁRIO",
        "camada": "TráfegoRodoviário",
        "nome": "EixoRua,Avenida,Rotunda,Praça,Largo,Passeio",
        "map": [
            {
                "table": "seg_via_rodov",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_estado_via_rodov",
                        "op": "set",
                        "value": 5
                    },
                    {
                        "src": "",
                        "dst": "valor_caract_fisica_via_rodov",
                        "op": "set",
                        "value": 3
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical_transportes",
                        "op": "set",
                        "value": 0
                    },
                    {
                        "src": "",
                        "dst": "valor_sentido",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_circulacao",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_troco_rodoviario",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "num_vias_transito",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "pavimentado",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "8010100": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DE ÁGUAS",
        "objeto": "APROVEITAMENTO DE ÁGUAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010601": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "OUTROS",
        "objeto": "ESTRADA ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061001": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "BOMBEIROS",
        "objeto": "QUARTEL DE BOMBEIROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6070103": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS COM OUTRAS UTILIZAÇÕES",
        "familia": "MOINHOS   (QUANDO N/ FOREM INDUSTRIAIS)",
        "objeto": "MOINHO sem representação à escala",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "20"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "6010210": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "POSTO DE TURISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010105": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FUNDAMENTAL",
        "objeto": "EIXO DO IP",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060303": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "UNIVERSIDADE/FACULDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060505": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "CULTURA",
        "objeto": "TEATRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13030200": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "ÁREAS  DE INTERESSE HISTÓRICO",
        "familia": null,
        "objeto": "RUINAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010901": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PARQUES DE DIVERSÃO",
        "objeto": "FEIRA POPULAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100502": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "CAMINHO FLORESTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13060000": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "ÁREAS PÚBLICAS E OFICIAIS",
        "familia": null,
        "objeto": "ÁREA DE UTILIZAÇÃO PÚBLICA E OFICIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010205": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "SARJETA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150403": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "RECOLHA DE RESÍDUOS SÓLIDOS",
        "objeto": "PAPELEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010201": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "CÂMARA MUNICIPAL, SECRETARIA REGIONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120514": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "DUNAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050802": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "FABRICO E ARMAZENAMENTO DE MATERIAIS EXPLOSIVOS",
        "objeto": "OFICINA DE PIROTECNIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050803": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "FABRICO E ARMAZENAMENTO DE MATERIAIS EXPLOSIVOS",
        "objeto": "PAIOL (DEPÓSITO DE MATERIAIS EXPLOSIVOS)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120106": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "INSTALAÇÕES DESPORTIVAS",
        "objeto": "OUTRAS CONSTRUÇÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6170000": {
        "dominio": "Construções",
        "subdominio": null,
        "familia": null,
        "objeto": "EDIFÍCIOS SUBTERRÂNEOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090114": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "VELÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010709": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "ANTENA DE EMISSÃO/RECEÇÃO   sem representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120503": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "CABO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070302": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO FERROVIÁRIO  ( METRO )",
        "objeto": "ESTAÇÃO SUPERFICIAL           (PLATAFORMA)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010303": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "OUTROS",
        "objeto": "DESIGNAÇÕES  LOCAIS (Pinhal, Mouchão, Lombo, etc.)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010516": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "AUDITÓRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061303": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "EDIFÍCIOS INDUSTRIAIS",
        "objeto": "CAIS   (EMBARQUE/DESEMBARQUE)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010903": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA SOLAR",
        "objeto": "HELIÓGRAFO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090202": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "Pinheiros",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "4.2.1"
                    }
                ]
            }
        ]
    },
    "12010504": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "AQUEDUTOS",
        "objeto": "AQUEDUTO SUBTERRÂNEO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010114": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "POSTO FRONTEIRIÇO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11040101": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "VINHAS",
        "familia": null,
        "objeto": "VINHA (LATADA)",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "1.3"
                    }
                ]
            }
        ]
    },
    "10030102": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO - METRO",
        "familia": "METRO",
        "objeto": "LINHA SUPERFICIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2020104": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "LIMITES DO INE",
        "objeto": "CÓDIGO DE SECÇÃO ESTATÍSTICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120106": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "CURSOS DE ÁGUA",
        "objeto": "VALA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020206": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "LOGRADOURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050109": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "VACARIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11030300": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS  HORTO-FRUTÍCOLAS",
        "familia": null,
        "objeto": "BANANAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080112": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PORTAGEM sem representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100102": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FUNDAMENTAL",
        "objeto": "IP / AE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10090101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS  DE APOIO AO TRÁFEGO MARÍTIMO|ÁREAS DE APOIO AO TRÁFEGO MARÍTIMO",
        "familia": null,
        "objeto": "PORTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010101": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "PARLAMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010313": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "EIXO DOS ARRUAMENTOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010204": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIOS",
        "objeto": "EIXO DO RIO NÃO NAVEGÁVEL NEM FLUTUÁVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010216": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "TEXTO ASSOCIADO A UM VÉRTICE GEODÉSICO *",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150303": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "ÁREAS COMERCIAIS",
        "objeto": "POSTO DE VENDA FIXO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4070501": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO",
        "familia": "DE RESÍDUOS",
        "objeto": "RESÍDUOS SÓLIDOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060203": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "TRIBUNAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080103": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARQUE DE ESTACIONAMENTO SUBTERRÂNEO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020204": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "ESCADAS",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "1.2"
                    }
                ]
            }
        ]
    },
    "12020601": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "TANQUES/LAGOS PEQUENOS",
        "objeto": "LAGO DE JARDIM",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 6
                    }
                ]
            }
        ]
    },
    "4060101": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "PARLAMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010402": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "CABO DE  TRANSPORTE AÉREO  DE BAIXA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020302": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "MARCAÇÕES",
        "objeto": "MARCO HECTOMÉTRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010504": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "EIXO DA ESTRADA FLORESTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010100": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "NASCENTES",
        "objeto": "NASCENTES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010704": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "ANTENA  DE EMISSÃO / RECEPÇÃO com representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3020201": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "LimiteSuperiorDeEscarpado(Natural)",
        "map": [
            {
                "table": "linha_de_quebra",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_classifica",
                        "op": "set",
                        "value": 6
                    },
                    {
                        "src": "",
                        "dst": "valor_natureza_linha",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "artificial",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "11090207": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA PERSISTENTE",
        "objeto": "AZINHEIRAS",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "4.1.2"
                    }
                ]
            }
        ]
    },
    "6020201": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "GARAGEM",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "1.2"
                    }
                ]
            }
        ]
    },
    "6080101": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARQUE DE ESTACIONAMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100302": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "ESTRADA MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150501": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": null,
        "objeto": "MARCO DO CORREIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060104": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "PENITENCIÁRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030206": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "COMBRO (Limite Inferior)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010707": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "MESQUITA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "19"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.2"
                    }
                ]
            }
        ]
    },
    "12050106": {
        "dominio": "Hidrografia",
        "subdominio": "HIDROGRAFIA COSTEIRA",
        "familia": "MARGENS CONSOLIDADAS, LINHAS DE COSTA",
        "objeto": "MARGEM INDETERMINADA (ARENOSA)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090501": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "ÁRVORES DISPERSAS",
        "objeto": "ÁRVORE DE FRUTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010502": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "POSTE DE BAIXA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010515": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "ANFITEATRO DE AR LIVRE",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "1"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.3"
                    },
                    {
                        "src": "",
                        "dst": "altura_edificio",
                        "op": "set",
                        "value": -99999
                    }
                ]
            }
        ]
    },
    "3010217": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "TEXTO ASSOCIADO AO PONTO DE COTA EM SOLEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010223": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "ACESSO IP/AE/IC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090209": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA PERSISTENTE",
        "objeto": "OUTRAS ESPÉCIES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9020101": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ZONAS VERDES",
        "familia": "PARQUES E JARDINS",
        "objeto": "CANTEIROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010503": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "POSTE DE ILUMINAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030201": {
        "tema": "OUTROS LIMITES",
        "camada": "OutrosLimites_Muros",
        "nome": "SebeOuValado",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 3
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "12020602": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "TANQUES/LAGOS PEQUENOS",
        "objeto": "TANQUE",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 4
                    }
                ]
            }
        ]
    },
    "9010603": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISCINAS",
        "objeto": "COMPLEXO BALNEAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010509": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "CINEMA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010801": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "SAÍDAS DE AR DO METRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010202": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "VILAS",
        "objeto": "VILA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090601": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "ARBUSTOS",
        "objeto": "HAKEAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1030101": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDE FOTOGRAMÉTRICA",
        "familia": "PONTOS FOTOGRAMÉTRICOS",
        "objeto": "PONTO FOTOGRAMÉTRICO TOTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050103": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "REFINARIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030210": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "DESATERRO (Limite Inferior)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010209": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "SEPARADOR / PROTECTOR ASSOCIADO AO IC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010406": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE DE UTILIZAÇÃO LIMITADA",
        "objeto": "EIXO DO CAMINHO MILITAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010602": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIBEIRAS /LINHAS DE ÁGUA",
        "objeto": "RIBEIRA/RIBEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010306": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": null,
        "objeto": "CURVA DE NÍVEL SECUNDÁRIA  EM ZONA COBERTA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100802": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "LOCAIS DE PARAGEM - ÁREAS DE APOIO AO TRÁFEGO ÁEREO",
        "objeto": "AERÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4062201": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "REPRESENTAÇÕES DIPLOMÁTICAS",
        "objeto": "EMBAIXADA, CONSULADO, MISSÃO, ETC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7050600": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "ÁREA DE ATRERRO SANITÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020201": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA ESTREITA",
        "objeto": "VIA DUPLA ELECTRIFICADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100101": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FUNDAMENTAL",
        "objeto": "IP",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4130303": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS COM OUTRAS UTILIZAÇÕES",
        "familia": "ÁREAS  DE INTERESSE HISTÓRICO",
        "objeto": "ESTAÇÕES ARQUEOLÓGICAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010603": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "PELOURINHO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010102": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "CIDADES",
        "objeto": "CAPITAL DE REGIÃO AUTÓNOMA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061301": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "EDIFÍCIOS INDUSTRIAIS",
        "objeto": "FÁBRICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010305": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "OUTROS",
        "objeto": "ALDEIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100304": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "CAMINHO VICINAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010601": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "MOSTEIRO, CONVENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010213": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "VÉRTICE DE ADENSAMENTO DA REDE GEODÉSICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020301": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "MARCAÇÕES",
        "objeto": "MARCO QUILOMÉTRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090105": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "AUTÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010803": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "MARCAÇÕES",
        "objeto": "TEXTO ASSOCIADO A UM MARCO RODOVIÁRIO*",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010606": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "FORTE",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "15"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "10010704": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "CROSS MOTORIZADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010405": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "MATERNIDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061604": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "ESTALEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010502": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "CAMINHO FLORESTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010110": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "LIMITES ADMINISTRATIVOS",
        "objeto": "LIMITE MILITAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020102": {
        "tema": "CONTRUÇÕES",
        "camada": "Construções",
        "nome": "Vivenda,Casa",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "1.1"
                    }
                ]
            }
        ]
    },
    "6011001": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "BOMBEIROS",
        "objeto": "QUARTEL DE BOMBEIROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120104": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "INSTALAÇÕES DESPORTIVAS",
        "objeto": "BANCADAS",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 8
                    }
                ]
            }
        ]
    },
    "3010213": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "TEXTO ASSOCIADO AO PONTO DE COTA NO PAVIMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11010101": {
        "tema": "ÁREAS ARÁVEIS",
        "camada": "AreasAgricolas",
        "nome": "Regadio/Horta",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "1.1"
                    }
                ]
            }
        ]
    },
    "9010306": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "CROSS MOTORIZADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120501": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "BOTÂNICA",
        "objeto": "EDIFÍCIOS ASSOCIADOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4130401": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS COM OUTRAS UTILIZAÇÕES",
        "familia": "CEMITÉRIOS",
        "objeto": "CEMITÉRIOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010507": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "OBSERVATÓRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060903": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "EDIFÍCIO GNR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100801": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "LOCAIS DE PARAGEM - ÁREAS DE APOIO AO TRÁFEGO ÁEREO",
        "objeto": "AEROPORTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010207": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "TEXTO ASSOCIADO AO PONTO DE COTA NO BEIRAL DO TELHADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9020000": {
        "tema": "ZONAS VERDES",
        "camada": "AreasLazerRecreio_ZonasVerdes",
        "nome": "ÁreaVerdeEmGeral",
        "map": [
            {
                "table": "areas_artificializadas",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_artificializadas",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "10010204": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "IC / CIRCULAR REGIONAL INTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020204": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA ESTREITA",
        "objeto": "VIA SIMPLES NÃO ELECTRIFICADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030401": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "BARREIRAS ACÚSTICAS",
        "objeto": "BARREIRA ACÚSTICA",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 7
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "12020405": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ESTRUTURAS ASSOCIADAS E OUTRAS",
        "objeto": "ECLUSA sem representação à escala  ??????????????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6110102": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES AGRO-FLORESTAIS",
        "familia": "SERVIÇOS FLORESTAIS",
        "objeto": "POSTO DE VIGIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010611": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "CASTRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100204": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEME NTAR",
        "objeto": "IC / CIRCULAR REGIONAL INTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7090101": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DE DEPÓSITO DE LIXOS",
        "familia": null,
        "objeto": "PARQUE DE SUCATA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010612": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "MENIR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080251": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO FERROVIÁRIO",
        "objeto": "ESTAÇÃO DO METRO SUPERFICIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030108": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "PILAR DE MURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010208": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DO IC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010408": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "ARMÁRIO DE ELETRICIDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6110210": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES AGRO-FLORESTAIS",
        "familia": "CONSTRUÇÕES RURAIS",
        "objeto": "ESPIGUEIRO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": {
                            "ndd1": "11"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "10120104": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "PASSAGENS NIVELADAS, DESNIVELADAS E TÚNEIS",
        "objeto": "TÚNEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010214": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DO IC/CIRC.REGIONAL INTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010705": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "BASÍLICA,CATEDRAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10090304": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS  DE APOIO AO TRÁFEGO MARÍTIMO|ÁREAS DE APOIO AO TRÁFEGO MARÍTIMO",
        "familia": "ESTRUTURAS DE APOIO Á NAVEGAÇÃO",
        "objeto": "MOLHE",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 1
                    }
                ]
            }
        ]
    },
    "10010309": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "EIXO DA EN",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "PONTES PARA TRÁFEGO",
        "objeto": "PONTE DE FERRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060204": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "GOVERNO CIVIL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020500": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIAS FERROVIÁRIAS EM CONSTRUÇÃO",
        "objeto": "VIAS FERROVIÁRIA EM CONSTRUÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020100": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "RESIDENCIAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010903": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "FORTE ??????????????????????????????????????",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "15"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "2030106": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURO COM JORRAMENTO",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 10
                    }
                ]
            }
        ]
    },
    "3010211": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "TEXTO ASSOCIADO AO PONTO DE COTA EM CUMEEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020109": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "PILAR QUADRADO DE CASA ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020203": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "ARRECADAÇÃO/ANEXO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "1.2"
                    }
                ]
            }
        ]
    },
    "9010602": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISCINAS",
        "objeto": "PISCINA DE ÁGUA NATURAL",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 3
                    }
                ]
            }
        ]
    },
    "9020301": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ZONAS VERDES",
        "familia": "JARDINS BOTÂNICOS",
        "objeto": "JARDIM BOTÂNICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050501": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "ARMAZENAGEM",
        "objeto": "ARMAZÉM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010708": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "EIXO DA PISTA DE CROSS MOTORIZADO",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 8
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "9010305": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "VELÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090208": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA PERSISTENTE",
        "objeto": "FLORESTA NATURAL DA MADEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010106": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FUNDAMENTAL",
        "objeto": "EIXO DO IP/AE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10090204": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS  DE APOIO AO TRÁFEGO MARÍTIMO|ÁREAS DE APOIO AO TRÁFEGO MARÍTIMO",
        "familia": "CAIS DE EMBARQUE",
        "objeto": "MARINA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010302": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "CANAIS",
        "objeto": "CANAIS SUBTERRÂNEOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020105": {
        "tema": "CONTRUÇÕES",
        "camada": "Construções",
        "nome": "BarracaComRepresEscala",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "4"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "1.2"
                    }
                ]
            }
        ]
    },
    "6010604": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "ALMINHA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020103": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "EDIFÍCIO COM MAIS DE DOIS PISOS",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "1.1"
                    }
                ]
            }
        ]
    },
    "9010000": {
        "tema": "ÁREAS DA LAZER E RECREIO",
        "camada": "AreasLazerRecreio_AreasDesportivas",
        "nome": "ÁreaDesportivaEmGeral",
        "map": [
            {
                "table": "areas_artificializadas",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_artificializadas",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "6150301": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "ÁREAS COMERCIAIS",
        "objeto": "ESPLANADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010201": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": " MARCO GEODÉSICO DE 1ª ORDEM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040100": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "COMUNICAÇÕES",
        "objeto": "COMUNICAÇÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7100101": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DE INDÚSTRIAS HOTELEIRAS",
        "familia": null,
        "objeto": "ÁREA DE ALDEAMENTO TURÍSTICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061606": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "objeto": "ESTAÇÃO DO METROPOLITANO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6100400": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "INSTALAÇÃO PARA TRATAMENTO DE RESÍDUOS TÓXICOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010201": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "IC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020207": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "RAMPA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010700": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "TELECOMUNICAÇÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010101": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "NASCENTES",
        "objeto": "NASCENTE A CÉU ABERTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040300": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": null,
        "objeto": "EDIFÍCIO DE ESCRITÓRIOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050700": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "MATÉRIAS EXTRATIVAS",
        "objeto": "MATÉRIAS EXTRATIVAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050108": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "INDÚSTRIA TRANSFORMADORA DE PRODUTOS AGRÍCOLAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030102": {
        "tema": "ALTIMETRIA 3D E 2D",
        "camada": "Altimetria_CurvasDeNivel_3D",
        "nome": "LimiteInferiorDeEscarpado(Artificial)",
        "map": [
            {
                "table": "linha_de_quebra",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_classifica",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "valor_natureza_linha",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "artificial",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "6010319": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "INSTITUTO POLITECNICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010904": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PARQUES DE DIVERSÃO",
        "objeto": "PARQUE TEMÁTICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060405": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "LAR 3ª IDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6070102": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS COM OUTRAS UTILIZAÇÕES",
        "familia": "MOINHOS   (QUANDO N/ FOREM INDUSTRIAIS)",
        "objeto": "AZENHA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "3"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "6010514": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "AQUÁRIO / OCEANÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150201": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "SINALIZAÇÃO",
        "objeto": "SEMÁFORO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090502": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "ÁRVORES DISPERSAS",
        "objeto": "PINHEIRO MANSO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010711": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "VELÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010505": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "AQUEDUTOS",
        "objeto": "RESPIRADOURO DE AQUEDUTO SUBTERRÂNEO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010616": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "RUÍNAS COM INTERESSE HISTÓRICO sem representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070103": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARAGEM DE AUTOCARRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010401": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "HOSPITAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10090303": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS  DE APOIO AO TRÁFEGO MARÍTIMO|ÁREAS DE APOIO AO TRÁFEGO MARÍTIMO",
        "familia": "ESTRUTURAS DE APOIO Á NAVEGAÇÃO",
        "objeto": "FAROL",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "6.6"
                    }
                ]
            }
        ]
    },
    "6050403": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "CENTRAIS DE ABASTECIMENTO",
        "objeto": "DEPÓSITO DE COMBUSTÍVEL PETRÓLEO E DERIVADOS LIQUIDOS E LIQUEFEITOS) s/ rep. À escala",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "26"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "10020100": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA LARGA",
        "objeto": "VIA LARGA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010314": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "EIXO DAS ESCADARIAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050400": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "CENTRAIS DE ABASTECIMENTO",
        "objeto": "CENTRAIS DE ABASTECIMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010203": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "EDIFICIO ADMINISTRATIVO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030115": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURO DE SUPORTE DEANCORADO (AE)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010407": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA ELÉCTRICA",
        "objeto": "CABO DE TRANSPORTE DE ALTA TENSÃO SUBTERRÂNEO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010209": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "TEXTO ASSOCIADO AO PONTO DE COTA EM POLÍGONO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120501": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "PRAIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010102": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "CAMPOS DE JOGOS",
        "objeto": "CAMPO DE JOGOS SEM BANCADAS",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 5
                    }
                ]
            }
        ]
    },
    "8010506": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA  ELÉCTRICA    ( POSTES )",
        "objeto": "POSTE DE MÉDIA TENSÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090100": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA CADUCA",
        "objeto": "FOLHA CADUCA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010401": {
        "tema": "ESTRUTURAS DE ABASTECIMENTO",
        "camada": "EstruturasAproveitamento",
        "nome": "CaboAéreoDeAltaTensão",
        "map": [
            {
                "table": "cabo_electrico",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_designacao_tensao",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical",
                        "op": "set",
                        "value": 1
                    }
                ]
            }
        ]
    },
    "10010216": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DO IC/CIR.REGIONAL EXTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020205": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "PÁTIO INTERIOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10040100": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ASCENSORES/ELEVADORES",
        "familia": "ELEVADORES",
        "objeto": "ELEVADORES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010315": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "CÓDIGO ASSOCIADO AO EIXO DE VIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1070104": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDES LOCAIS",
        "familia": "REDES LOCAIS PRINCIPAIS",
        "objeto": "TEXTO ASSOCIADO AO VÉRTICE DA REDE PRINCIPAL ALTIMÉTRICA *",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010214": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": "PONTO DE COTA EM TABULEIRO DE PONTE E OUTRAS VIAS ELEVADAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6070101": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS COM OUTRAS UTILIZAÇÕES",
        "familia": "MOINHOS   (QUANDO N/ FOREM INDUSTRIAIS)",
        "objeto": "MOINHO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "20"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "10010307": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "ARRUAMENTOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070200": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO FERROVIÁRIO  (C.  F. )",
        "objeto": "TRÁFEGO FERROVIÁRIO (C.F.)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010206": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "GOVERNO CIVIL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020100": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "COMPORTAS",
        "objeto": "COMPORTAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020200": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA ESTREITA",
        "objeto": "VIA ESTREITA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11010100": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS  ARÁVEIS",
        "familia": "CULTURA ARVENSE",
        "objeto": "CULTURA ARVENSE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020203": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ACUDES/REPRESAS",
        "objeto": "DIQUE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010203": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "IC / VIA RÁPIDA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010409": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "CENTRO DE DIAGNÓSTICO COMPLEMENTAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10080301": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO ÁEREO",
        "familia": "HELIPORTOS",
        "objeto": "HELIPORTO SOBRE SOLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010311": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "EIXO DO CAMINHO MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090201": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "PARQUES E JARDINS",
        "objeto": "PARQUES E JARDINS ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7070101": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DE ABASTECIMENTO DE GÁS",
        "familia": null,
        "objeto": "CENTRAL DE ABASTECIMENTO DE GÁS / TERMINAL DE ARMAZENAGEM DE GÁS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080283": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO FERROVIÁRIO",
        "objeto": "ESTAÇÃO DO METRO SUBTERRÂNEA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010614": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "MURALHA",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 5
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "4060205": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "JUNTA DE FREGUESIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6100300": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "INSTALAÇÃO PARA TRATAMENTO DE RESÍDUOS INDUSTRIAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010703": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "SANTUÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090108": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "CARREIRA DE TIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11070101": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": null,
        "familia": "EXPLORAÇÕES  AGRÍCOLAS",
        "objeto": "ARROZAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10090201": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS  DE APOIO AO TRÁFEGO MARÍTIMO|ÁREAS DE APOIO AO TRÁFEGO MARÍTIMO",
        "familia": "CAIS DE EMBARQUE",
        "objeto": "CAIS FLUVIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020203": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA ESTREITA",
        "objeto": "VIA SIMPLES ELECTRIFICADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061405": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "CASA RURAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4020103": {
        "dominio": "Toponímia",
        "subdominio": "SERRAS",
        "familia": null,
        "objeto": "SERRA PEQUENA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010709": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "KARTÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12040601": {
        "dominio": "Hidrografia",
        "subdominio": "SUPERFÍCIES AQUÁTICAS",
        "familia": "PÂNTANOS",
        "objeto": "PÂNTANO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060602": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "MONUMENTOS",
        "objeto": "PADRÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010101": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "LIMITES ADMINISTRATIVOS",
        "objeto": "LIMITE DE PAÍS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010301": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "HIPÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110203": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "OUTRAS PONTES",
        "objeto": "PONTÃO",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 2
                    }
                ]
            }
        ]
    },
    "10010306": {
        "tema": "VIAS COMUNICAÇÃO - TRÁFEGO RODOVIÁRIO",
        "camada": "TráfegoRodoviário",
        "nome": "Rua,Avenida,Rotunda,Praça,Largo,Passeio",
        "map": [
            {
                "table": "via_rodov_limite",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    }
                ]
            }
        ]
    },
    "6090104": {
        "tema": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "camada": "InstalaçõesDestinadasAoAbastecimentoAgua",
        "nome": "DepósitoDeÁguaDeSuperfícieComRepresEscala",
        "map": [
            {
                "table": "elem_assoc_agua",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_associado_agua",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "10020303": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "MARCAÇÕES",
        "objeto": "TEXTO ASSOCIADO A UM MARCO FERROVIÁRIO*",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10120106": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "PASSAGENS NIVELADAS, DESNIVELADAS E TÚNEIS",
        "objeto": "PASSAGEM INFERIOR DE PEÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050202": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "PENSÃO,RESIDENCIAL, ALBERGARIA,ESTALEGEM   ???????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6140000": {
        "dominio": "Construções",
        "subdominio": null,
        "familia": "EDIFÍCIOS EM CONSTRUÇÃO",
        "objeto": "EDIFÍCIO EM CONSTRUÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010211": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "SEPARADOR / PROTECTOR ASSOCIADO AO IC/AE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010900": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "FORÇAS MILITARES OU MILITARIZADAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020202": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA ESTREITA",
        "objeto": "VIA DUPLA NÃO ELECTRIFICADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010103": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FUNDAMENTAL",
        "objeto": "SEPARADOR / PROTECTOR ASSOCIADO AO IP",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030304": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "VEDAÇÕES",
        "objeto": "VEDAÇÃO DE ARAME (AE)",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "4060607": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "MONUMENTOS",
        "objeto": "CASTRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010301": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DA ENERGIA EÓLICA|APROVEITAMENTO DE ENERGIA EÓLICA",
        "objeto": "AEROMOTOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010802": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "MARCAÇÕES",
        "objeto": "MARCO HECTOMÉTRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120502": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "PORTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150202": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "SINALIZAÇÃO",
        "objeto": "ARMÁRIO DE SEMÁFOROS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090210": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA PERSISTENTE",
        "objeto": "PINHAL DISPERSO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010203": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIOS",
        "objeto": "EIXO DO RIO NAVEGÁVEL OU FLUTUÁVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020400": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ESTRUTURAS ASSOCIADAS E OUTRAS",
        "objeto": "ESTRUTURAS ASSOCIADAS E OUTRAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7050300": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO DE RESÍDUOS",
        "familia": null,
        "objeto": "ÁREA PARA TRATAMENTO DE RESÍDUOS INDUSTRIAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010701": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "LOCAIS DE CULTO",
        "objeto": "IGREJA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "17"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.2"
                    }
                ]
            }
        ]
    },
    "6010808": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDIFÍCIOS SOCIAIS",
        "objeto": "ATL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010230": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "ESTRADA REGIONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010210": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DO IC/AE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050101": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "MATADOURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7010301": {
        "dominio": "Indústrias",
        "subdominio": "EXPLORAÇÕES MINEIRAS",
        "familia": null,
        "objeto": "PEDREIRA (Massas Minerais)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120509": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "ENSEADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010305": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": null,
        "objeto": "CURVA DE NÍVEL MESTRA EM ZONA COBERTA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4061402": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "POUSADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9020103": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ZONAS VERDES",
        "familia": "PARQUES E JARDINS",
        "objeto": "PARQUES E JARDINS PRIVADOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2010105": {
        "dominio": "Limites",
        "subdominio": "LIMITES ADMINISTRATIVOS, FISCAIS E JURÍDICOS",
        "familia": "LIMITES ADMINISTRATIVOS",
        "objeto": "LIMITE DE DISTRITO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010902": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ENERGIA SOLAR",
        "objeto": "ÁREA CONTENDO PAINEIS SOLARES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010803": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDIFÍCIOS SOCIAIS",
        "objeto": "LAR DA 3ª IDADE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110102": {
        "tema": "OBRAS DE ARTE - PONTES",
        "camada": "ObrasArte_PontesParaTrafego",
        "nome": "PonteDeCantariaOuBetãoArmado",
        "map": [
            {
                "table": "obra_arte",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_obra_arte",
                        "op": "set",
                        "value": 1
                    }
                ]
            }
        ]
    },
    "10010703": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "AUTÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010401": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PRAÇAS DE TOUROS",
        "objeto": "PRAÇA DE TOUROS",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "23"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "8.4"
                    }
                ]
            }
        ]
    },
    "11070201": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": null,
        "familia": "EIRAS",
        "objeto": "EIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12050102": {
        "dominio": "Hidrografia",
        "subdominio": "HIDROGRAFIA COSTEIRA",
        "familia": "MARGENS CONSOLIDADAS, LINHAS DE COSTA",
        "objeto": "OBLÍQUA (PAREDÃO)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010804": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDIFÍCIOS SOCIAIS",
        "objeto": "CENTRO SOCIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090101": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "ESTAÇÃO DE TRATAMENTO ÁGUAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10050104": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "OUTROS   ????????????????????????????",
        "objeto": "EIXO DA CICLOVIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090111": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "BOCA DE INCÊNDIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010615": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "CENTRO HISTÓRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010102": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "NASCENTES",
        "objeto": "NASCENTE MINERAL A CÉU ABERTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010705": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "EIXO DE PISTA DE TRÁFEGO AÉREO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090113": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "PISTA DE MOTOCICLISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010603": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "OUTROS",
        "objeto": "EIXO DA ESTRADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010402": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PRAÇAS DE TOUROS",
        "objeto": "LIMITE DA ARENA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010116": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "PROTEÇÃO CIVIL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11030200": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS  HORTO-FRUTÍCOLAS",
        "familia": null,
        "objeto": "OLIVAL",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "1.5"
                    }
                ]
            }
        ]
    },
    "11050100": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS DE PASTAGEM",
        "familia": null,
        "objeto": "PASTAGENS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010501": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "ESTRADA FLORESTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1030103": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDE FOTOGRAMÉTRICA",
        "familia": "PONTOS FOTOGRAMÉTRICOS",
        "objeto": "PONTO FOTOGRAMÉTRICO ALTIMÉTRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7100102": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS DE INDÚSTRIAS HOTELEIRAS",
        "familia": null,
        "objeto": "ÁREA DE CONJUNTO TURÍSTICO (RESORT)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010104": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "CURVAS DE NÍVEL",
        "objeto": "INFORMAÇÃO ALEATÓRIA   ??????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010506": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "CULTURA",
        "objeto": "PLANETÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030204": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "ATERRO/DESATERRO LIMITE INFERIOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010505": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE FLORESTAL",
        "objeto": "EIXO DO CAMINHO FLORESTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030116": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURETE DE LUZ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10100104": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO TERRESTRE",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARQUE DE ESTACIONAMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010707": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "CABINA TELEFÓNICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050206": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "MOTEL   ???????????????????????????????????",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080110": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PORTAGEM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060202": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "ADMINISTRAÇÃO REGIONAL",
        "objeto": "ASSEMBLEIA MUNICIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010317": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "RUA PEDONAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010306": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA SECUNDÁRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10030101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO - METRO",
        "familia": "METRO",
        "objeto": "LINHA SUBTERRÂNEA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010205": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "IC / CIRCULAR REGIONAL EXTERNA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120100": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "INSTALAÇÕES DESPORTIVAS",
        "objeto": "INSTALAÇÕES DESPORTIVAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4020101": {
        "dominio": "Toponímia",
        "subdominio": "SERRAS",
        "familia": null,
        "objeto": "SERRA PRINCIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020105": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA LARGA",
        "objeto": "VIA SIMPLES NÃO ELECTRIFICADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080102": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO RODOVIÁRIO",
        "objeto": "PARQUE DE ESTACIONAMENTO EM TELHEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010201": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "COLECTOR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010106": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "APROVEITAMENTO DE ÁGUAS",
        "objeto": "LEVADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120102": {
        "tema": "INSTALAÇÕES DE RECREIO REPOUSO",
        "camada": "InstalacoesDeRecreioRepouso",
        "nome": "Piscina",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 3
                    }
                ]
            }
        ]
    },
    "4120101": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "CURSOS DE ÁGUA",
        "objeto": "RIO PRINCIPAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020604": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "TANQUES/LAGOS PEQUENOS",
        "objeto": "TANQUE sem representação à escala",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 4
                    }
                ]
            }
        ]
    },
    "9010307": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "ATLETISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010401": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE DE UTILIZAÇÃO LIMITADA",
        "objeto": "ESTRADA MILITAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150300": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "ÁREAS COMERCIAIS",
        "objeto": "ÁREAS COMERCIAIS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020102": {
        "tema": "TRÁFEGO FERROVIÁRIO",
        "camada": "TráfegoFerroviário",
        "nome": "ViaLargaDuplaElectrificada",
        "map": [
            {
                "table": "seg_via_ferrea",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_categoria_bitola",
                        "op": "set",
                        "value": 995
                    },
                    {
                        "src": "",
                        "dst": "valor_estado_linha_ferrea",
                        "op": "set",
                        "value": 5
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical_transportes",
                        "op": "set",
                        "value": 0
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_linha_ferrea",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_troco_via_ferrea",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "eletrific",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "6020200": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "ANEXOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120602": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "EXCURSIONISMO/PEDESTRIANISMO",
        "objeto": "PLACA INFORMATIVA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090204": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ELECTRICIDADE ",
        "objeto": "POSTO DE TRANSFORMAÇÃO sem representação à escala",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010207": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "CAIXA DE VISITA UNITÁRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010408": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE DE UTILIZAÇÃO LIMITADA",
        "objeto": "EIXO DO CAMINHO PARTICULAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060404": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "CENTRO DE SAÚDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030301": {
        "tema": "OUTROS LIMITES",
        "camada": "OutrosLimites_Muros",
        "nome": "VedaçãoDeArameOuRede",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "2030111": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURO DE SUPORTE DE TERRA ARMADA (AE)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030104": {
        "tema": "OUTROS LIMITES",
        "camada": "OutrosLimites_Muros",
        "nome": "MuroDeSuporteDeAlvernaria",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 1
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "10010710": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "PISTAS",
        "objeto": "PISTA DE MOTOCICLISMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13030000": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "ÁREAS  DE INTERESSE HISTÓRICO",
        "familia": null,
        "objeto": "ÁREAS DE INTERESSE HISTÓRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040101": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "COMUNICAÇÕES",
        "objeto": "CORREIOS,TELÉGRAFOS E TELEFONES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090201": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA PERSISTENTE",
        "objeto": "EUCALIPTOS",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "4.1.5"
                    }
                ]
            }
        ]
    },
    "12010603": {
        "tema": "NASCENTES E CURSOS DE ÁGUA",
        "camada": "NascentesCursosDeAgua_Rios_2D/3D",
        "nome": "EixoLinhaDeAgua_2D",
        "map": [
            {
                "table": "curso_de_agua_eixo",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_curso_de_agua",
                        "op": "set",
                        "value": 4
                    },
                    {
                        "src": "",
                        "dst": "valor_posicao_vertical",
                        "op": "set",
                        "value": 0
                    },
                    {
                        "src": "",
                        "dst": "delimitacao_conhecida",
                        "op": "set",
                        "value": true
                    },
                    {
                        "src": "",
                        "dst": "ficticio",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "11090203": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA PERSISTENTE",
        "objeto": "PALMEIRAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120201": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "MANCHAS DE ÁGUA",
        "objeto": "LAGOA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090111": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "PARQUE AQUÁTICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010903": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PARQUES DE DIVERSÃO",
        "objeto": "PARQUE INFANTIL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010910": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "FORÇAS MILITARES OU MILITARIZADAS",
        "objeto": "CAPITANIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010104": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "CIDADES",
        "objeto": "SEDE DE CONCELHO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10070203": {
        "dominio": "Vias de Comunicação",
        "subdominio": "LOCAIS DE PARAGEM",
        "familia": "TRÁFEGO FERROVIÁRIO  (C.  F. )",
        "objeto": "ESTAÇÃO DE MERCADORIAS   (PLATAFORMA)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13100000": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": null,
        "familia": "FEIRAS",
        "objeto": "RECINTO DE FEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1030102": {
        "dominio": "Redes Geodésicas",
        "subdominio": "REDE FOTOGRAMÉTRICA",
        "familia": "PONTOS FOTOGRAMÉTRICOS",
        "objeto": "PONTO FOTOGRAMÉTRICO PLANIMÉTRCO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030112": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "MURO DE SUPORTE DE GABIÕES (AE)",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": true
                    }
                ]
            }
        ]
    },
    "6050500": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "ARMAZENAGEM",
        "objeto": "ARMAZENAGEM",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050208": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "INDÚSTRIAS HOTELEIRAS",
        "objeto": "ALBERGUE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6120101": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "familia": "INSTALAÇÕES DESPORTIVAS",
        "objeto": "PAVILHÃO GIMNO-DESPORTIVO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090500": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "ÁRVORES DISPERSAS",
        "objeto": "ÁRVORE ISOLADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3010202": {
        "dominio": "Altimetria",
        "subdominio": "ALTIMETRIA",
        "familia": "PONTOS COTADOS",
        "objeto": " TEXTO ASSOCIADO AO PONTO COTADO  *",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10090202": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS  DE APOIO AO TRÁFEGO MARÍTIMO|ÁREAS DE APOIO AO TRÁFEGO MARÍTIMO",
        "familia": "CAIS DE EMBARQUE",
        "objeto": "CAIS MARÍTIMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030207": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "ATERRO (Limite  Superior)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010301": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "OUTROS",
        "objeto": "SEDE DE FREGUESIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120512": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "ILHA (ARQUIPÉLAGO)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010117": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "ADMINISTRAÇÃO CENTRAL",
        "objeto": "MINISTÉRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090105": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA CADUCA",
        "objeto": "ACÁCIAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010219": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "SEPARADOR / PROTECTOR ASSOCIADO IC/RADIAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060704": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "LOCAIS DE CULTO",
        "objeto": "IGREJA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030109": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "MUROS",
        "objeto": "PILAR CIRCULAR DE MURO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060402": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "SANATÓRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090119": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "CONDUTA NO SOLO A CÉU ABERTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020205": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "ACUDES/REPRESAS",
        "objeto": "REPRESA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040200": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "OUTROS SERVIÇOS",
        "objeto": "OUTROS SERVIÇOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11030100": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS  HORTO-FRUTÍCOLAS",
        "familia": null,
        "objeto": "POMAR",
        "map": [
            {
                "table": "area_agricola_florestal_mato",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_agricolas_florestais_matos",
                        "op": "set",
                        "value": "1.4"
                    }
                ]
            }
        ]
    },
    "10010301": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE MUNICIPAL",
        "objeto": "EN",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020103": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "VIA LARGA",
        "objeto": "VIA DUPLA NÃO ELETRIFICADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020209": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "ANEXOS",
        "objeto": "CHAMINÉ",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010303": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "KARTÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7060102": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS  DE PRODUÇÃO DE ENERGIA ELÉCTRICA",
        "familia": null,
        "objeto": "SUBESTAÇÃO ELÉTRICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010800": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": null,
        "objeto": "CAIXAS DE VISITAS INDIFERENCIADAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010203": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "TAMPA DE CAIXA DE VÁLVULAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010404": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "SAÚDE/HIGIENE",
        "objeto": "SANATÓRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6110211": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES AGRO-FLORESTAIS",
        "familia": "CONSTRUÇÕES RURAIS",
        "objeto": "ESTUFA AGRÍCOLA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "13"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "9020100": {
        "tema": "ZONAS VERDES",
        "camada": "AreasLazerRecreio_ZonasVerdes",
        "nome": "ParquesEJardinsEmGeral",
        "map": [
            {
                "table": "areas_artificializadas",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_areas_artificializadas",
                        "op": "set",
                        "value": 7
                    }
                ]
            }
        ]
    },
    "6050107": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "PRODUÇÃO / TRANSFORMAÇÃO",
        "objeto": "CHAMINÉ DE FÁBRICA",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "9"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "3"
                    }
                ]
            }
        ]
    },
    "3030212": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "LIMITE INFERIOR INDIFERENCIADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6050701": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS INDUSTRIAIS",
        "familia": "MATÉRIAS EXTRATIVAS",
        "objeto": "MINA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6040207": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS CENTRAIS LIGADOS A SERVIÇOS",
        "familia": "OUTROS SERVIÇOS",
        "objeto": "FARMÁCIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12040000": {
        "dominio": "Hidrografia",
        "subdominio": "SUPERFÍCIES AQUÁTICAS",
        "familia": null,
        "objeto": "SUPERFÍCIES AQUÁTICAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13070100": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": null,
        "familia": null,
        "objeto": "ÁREAS MILITARES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120204": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "MANCHAS DE ÁGUA",
        "objeto": "AÇUDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030205": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "SOCALCO (Limite inferior)",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090302": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "FOLHA CADUCA / FOLHA PERSISTENTE",
        "objeto": "ARVOREDO DENSO NÃO CLASSIFICADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13090101": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": null,
        "familia": "ÁREAS DE ANIMAIS",
        "objeto": "CANIL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020101": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "PALÁCIO",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "21"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "4062104": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "INSTALAÇÕES DE RECREIO/REPOUSO",
        "objeto": "POUSADA DA JUVENTUDE",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060605": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "MONUMENTOS",
        "objeto": "PALÁCIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010202": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIOS",
        "objeto": "RIO NÃO NAVEGÁVEL NEM FLUTUÁVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10080201": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO ÁEREO",
        "familia": "AERODROMOS",
        "objeto": "AERÓDROMO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060702": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "LOCAIS DE CULTO",
        "objeto": "SANTUÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4100201": {
        "dominio": "Toponímia",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEME NTAR",
        "objeto": "IC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020112": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "CORPO BALANÇADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10050102": {
        "dominio": "Vias de Comunicação",
        "subdominio": null,
        "familia": "OUTROS   ????????????????????????????",
        "objeto": "CICLOVIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12050104": {
        "dominio": "Hidrografia",
        "subdominio": "HIDROGRAFIA COSTEIRA",
        "familia": "MARGENS CONSOLIDADAS, LINHAS DE COSTA",
        "objeto": "MARGEM INSTÁVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10110107": {
        "dominio": "Vias de Comunicação",
        "subdominio": "OBRAS DE ARTE",
        "familia": "PONTES PARA TRÁFEGO",
        "objeto": "PONTES SOBREPOSTAS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6160104": {
        "dominio": "Construções",
        "subdominio": null,
        "familia": "DETALHES",
        "objeto": "CANTEIRO EMPEDRADO DE DECORAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010325": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "INSTITUTO DE INVESTIGAÇÃO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010222": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO RODOVIÁRIO",
        "familia": "REDE COMPLEMENTAR",
        "objeto": "EIXO DO ACESSO IP/AE/IC",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010205": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "RIOS",
        "objeto": "LIMITE DE NAVEGABILIDADE DOS RIOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080202": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO FERROVIÁRIO",
        "objeto": "APEADEIRO DO CF",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "8010208": {
        "dominio": "Estruturas de Abastecimento e Transporte",
        "subdominio": "ESTRUTURAS DE ABASTECIMENTO",
        "familia": "ESGOTOS",
        "objeto": "ESTAÇÃO ELEVATÓRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2040102": {
        "dominio": "Limites",
        "subdominio": null,
        "familia": "PORTÕES",
        "objeto": "PORTÃO DE AUTOESTRADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090301": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "GÁS",
        "objeto": "CENTRAL DE ABASTECIMENTO DE GÁS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090202": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "PARQUES E JARDINS",
        "objeto": "JARDIM ZOOLÓGICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12020102": {
        "dominio": "Hidrografia",
        "subdominio": "ESTRUTURAS",
        "familia": "COMPORTAS",
        "objeto": "COMPORTA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "13030100": {
        "dominio": "Áreas com outras utilizações",
        "subdominio": "ÁREAS  DE INTERESSE HISTÓRICO",
        "familia": null,
        "objeto": "MONUMENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10010404": {
        "tema": "VIAS COMUNICAÇÃO - TRÁFEGO RODOVIÁRIO",
        "camada": "TráfegoRodoviário",
        "nome": "CaminhoParticular",
        "map": [
            {
                "table": "area_infra_trans_rodov",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    }
                ]
            }
        ]
    },
    "10010104": {
        "tema": "VIAS COMUNICAÇÃO - TRÁFEGO RODOVIÁRIO",
        "camada": "TráfegoRodoviário",
        "nome": "IP/AE-Separador/Protector",
        "map": [
            {
                "table": "via_rodov_limite",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_limite",
                        "op": "set",
                        "value": 2
                    }
                ]
            }
        ]
    },
    "6080401": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO MARÍTIMO",
        "objeto": "ESTALEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030306": {
        "dominio": "Limites",
        "subdominio": "OUTROS LIMITES",
        "familia": "VEDAÇÕES",
        "objeto": "GUARDA CORPOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6020113": {
        "dominio": "Construções",
        "subdominio": "HABITAÇÕES",
        "familia": "RESIDENCIAIS",
        "objeto": "BARRACA sem representação à escala",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "4"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "888"
                    }
                ]
            }
        ]
    },
    "6020202": {
        "tema": "CONTRUÇÕES",
        "camada": "Construções",
        "nome": "Telheiro",
        "map": [
            {
                "table": "edificio",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_xy",
                        "op": "set",
                        "value": {
                            "ndd1": "4",
                            "ndd2": "6"
                        }
                    },
                    {
                        "src": "",
                        "dst": "valor_elemento_edificio_z",
                        "op": "set",
                        "value": "14"
                    },
                    {
                        "src": "",
                        "dst": "valor_forma_edificio",
                        "op": "set",
                        "value": "27"
                    },
                    {
                        "src": "",
                        "dst": "valor_utilizacao_atual",
                        "op": "set",
                        "value": "1.2"
                    }
                ]
            }
        ]
    },
    "12040201": {
        "dominio": "Hidrografia",
        "subdominio": "SUPERFÍCIES AQUÁTICAS",
        "familia": "LAGOS ARTIFICIAIS",
        "objeto": "ALBUFEIRA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10020401": {
        "dominio": "Vias de Comunicação",
        "subdominio": "TRÁFEGO FERROVIÁRIO",
        "familia": "ELÉTRICOS",
        "objeto": "LINHA DE ELÉTRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "12010802": {
        "dominio": "Hidrografia",
        "subdominio": "NASCENTES E CURSOS DE ÁGUA",
        "familia": "FORMAS ESPECIAIS",
        "objeto": "QUEDA DE ÁGUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010304": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA BÁSICA INTEGRADA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120504": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "PONTA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "2030102": {
        "tema": "OUTROS LIMITES",
        "camada": "OutrosLimites_Muros",
        "nome": "MuroDePedraSolta",
        "map": [
            {
                "table": "constru_linear",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_construcao_linear",
                        "op": "set",
                        "value": 2
                    },
                    {
                        "src": "",
                        "dst": "suporte",
                        "op": "set",
                        "value": false
                    }
                ]
            }
        ]
    },
    "4010302": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "OUTROS",
        "objeto": "LUGARES, CASAIS E OUTRAS POVOAÇÕES",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4070601": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DESTINADAS A TRATAMENTO",
        "familia": "ÁREA DE PRODUÇÃO DE ENERGIA ELÉCTRICA",
        "objeto": "CENTRAL ELÉCTRICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4010101": {
        "dominio": "Toponímia",
        "subdominio": "LOCALIDADES",
        "familia": "CIDADES",
        "objeto": "CAPITAL DE PAÍS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "3030211": {
        "dominio": "Altimetria",
        "subdominio": "ACIDENTES TOPOGRÁFICOS (ORIGEM ARTIFICIAL)",
        "familia": "ATERROS/ DESATERROS/SOCALCOS",
        "objeto": "LIMITE SUPERIOR INDIFERENCIADO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080308": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO AÉREO",
        "objeto": "TORRE DE RADAR",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010205": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM CASA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120513": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "ILHA, ILHEU",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010310": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLA BÁSICA DO 3º CICLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010324": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "SEMINÁRIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6150204": {
        "dominio": "Construções",
        "subdominio": "EQUIPAMENTO URBANO",
        "familia": "SINALIZAÇÃO",
        "objeto": "SINAL DE TRÂNSITO HORIZONTAL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090115": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "MARCO DE INCÊNDIO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "9010308": {
        "dominio": "Áreas de lazer e Recreio",
        "subdominio": "ÁREAS DESPORTIVAS",
        "familia": "PISTAS",
        "objeto": "PICADEIRO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6010609": {
        "dominio": "Construções",
        "subdominio": "EDIFÍCIOS PÚBLICOS E OFICIAIS",
        "familia": "PATRIMÓNIO CULTURAL E ARQUIOLÓGICO",
        "objeto": "RUÍNAS C/INTER. HISTÓRICO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120508": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "BAÍA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060302": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "EDUCAÇÃO/INVESTIGAÇÃO",
        "objeto": "ESCOLAS PRIMÁRIA ,PREPARATÓRIA E OU SECUNDÁRIA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "7060101": {
        "dominio": "Indústrias",
        "subdominio": "ÁREAS  DE PRODUÇÃO DE ENERGIA ELÉCTRICA",
        "familia": null,
        "objeto": "CENTRAL ELÉCTRICA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "1010206": {
        "dominio": "Redes Geodésicas",
        "subdominio": " REDE GEODÉSICA",
        "familia": " SINAIS GEODÉSICOS",
        "objeto": "GEODÉSICO EM MOINHO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4120511": {
        "dominio": "Toponímia",
        "subdominio": "HIDROGRAFIA",
        "familia": "TOPONIMIA COSTEIRA",
        "objeto": "OCEANO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4090102": {
        "dominio": "Toponímia",
        "subdominio": "ÁREAS DE LAZER E DE RECREIO",
        "familia": "ÁREAS DESPORTIVAS",
        "objeto": "CAMPOS DE JOGOS",
        "map": [
            {
                "table": "constru_polig",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    },
                    {
                        "src": "",
                        "dst": "inicio_objeto",
                        "op": "dnow"
                    },
                    {
                        "src": "",
                        "dst": "valor_tipo_construcao",
                        "op": "set",
                        "value": 5
                    }
                ]
            }
        ]
    },
    "12040603": {
        "dominio": "Hidrografia",
        "subdominio": "SUPERFÍCIES AQUÁTICAS",
        "familia": "PÂNTANOS",
        "objeto": "TERRENO INUNDÁVEL",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6080305": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DE APOIO AO TRÁFEGO",
        "familia": "TRÁFEGO AÉREO",
        "objeto": "TORRE DE CONTROLO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090304": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "GÁS",
        "objeto": "ARMAZÉM DE GARRAFAS DE GÁS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "4060601": {
        "dominio": "Toponímia",
        "subdominio": "CONSTRUÇÕES",
        "familia": "MONUMENTOS",
        "objeto": "MOSTEIRO, CONVENTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090118": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "FONTE DE MERGULHO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "11090600": {
        "dominio": "Áreas Agrícolas e Florestais",
        "subdominio": "ÁREAS FLORESTAIS",
        "familia": "ARBUSTOS",
        "objeto": "ARBUSTOS",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "6090100": {
        "dominio": "Construções",
        "subdominio": "INSTALAÇÕES DESTINADAS AO ABASTECIMENTO",
        "familia": "ÁGUA",
        "objeto": "ÁGUA",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    },
    "10080101": {
        "dominio": "Vias de Comunicação",
        "subdominio": "ÁREAS DE APOIO AO TRÁFEGO ÁEREO",
        "familia": "AEROPORTOS",
        "objeto": "AEROPORTO",
        "map": [
            {
                "table": "",
                "fields": [
                    {
                        "src": "1_geom",
                        "dst": "geometria",
                        "op": "eq"
                    }
                ]
            }
        ]
    }
}
