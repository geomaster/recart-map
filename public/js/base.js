var recartBase = {
    "area_infra_trans_cabo": {
        "tema": "transportes (transporte por cabo)",
        "geo_caract": "área da infraestrutura de transporte por cabo",
        "def": "área que dispõe de instalações, equipamentos e serviços destinados ao transporte por cabo.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "fronteira_terra_agua": {
        "tema": "hidrografia",
        "geo_caract": "fronteira terra-água",
        "def": "linha que estabelece a fronteira entre a terra e a água e que representa a “linha de costa” assim como o limite das ilhas em águas interiores e no mar.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_fonte_dados",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "ilha",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "area_trabalho": {
        "tema": "auxiliar",
        "geo_caract": "área de trabalho",
        "def": "perímetro correspondente ao âmbito territorial dos dados associados.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "datahomologacao",
                "mult": "1",
                "tipo": "Data",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nivel_de_detalhe",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome_do_produtor",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome_do_proprietario",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "sinal_geodesico": {
        "tema": "construções",
        "geo_caract": "sinal geodésico",
        "def": "ponto da rede geodésica nacional cujas coordenadas geográficas e posição relativa, referidas a um elipsoide de referência, são conhecidas com grande exatidão. a coordenada altimétrica é representada através da altitude ortométrica do topo do sinal geodésico.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_local_geodesico",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_ordem",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_sinal_geodesico",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_revisao",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_local_geodesico",
                "vals": [
                    {
                        "val": "1",
                        "def": "sinal geodésico em torre, cruz ou zimbório da igreja.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "sinal geodésico em eixo do catavento.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "sinal geodésico em torre de edifício, ou no telhado de uma construção.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "sinal geodésico sobre moinho.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "sinal geodésico no topo de cruzeiro.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "sinal geodésico construído num ponto de um castelo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "sinal geodésico construído no topo de um depósito de água elevado.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "sinal geodésico construído no topo ou num ponto de um farol.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "sinal geodésico construído no topo de um posto de vigia.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "10",
                        "def": "sinal geodésico na ponta de para-raios.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "11",
                        "def": "sinal geodésico sobre o terreno.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "995",
                        "def": "",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_ordem",
                "vals": [
                    {
                        "val": "1",
                        "def": "pontos da rede geodésica nacional espaçados entre si de 30 a 60 km, cujas coordenadas geográficas e posição relativa, referidas a um elipsoide de referência, são conhecidas com grande exatidão.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "pontos da rede geodésica nacional espaçados entre si de 20 a 30 km e 5 a 10 km, cujas coordenadas geográficas e posição relativa, referidas a um elipsoide de referência, são conhecidas com grande exatidão.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "995",
                        "def": "",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_sinal_geodesico",
                "vals": [
                    {
                        "val": "1",
                        "def": "estação permanente gnss (global navigation satellite system)",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "vértice geodésico",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "marca de nivelamento",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "marégrafo",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "estação gravimétrica",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "edificio": {
        "tema": "construções",
        "geo_caract": "edifício",
        "def": "construção em alvenaria, betão ou outro material, com um ou vários pisos, de caráter permanente, em geral limitada por paredes e teto, que serve de habitação ou constitui um espaço comercial, industrial, administrativo, religioso, cultural, etc.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_condicao_const",
                "mult": "[0..1]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_elemento_edificio_xy",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_elemento_edificio_z",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_forma_edificio",
                "mult": "[0..1]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_utilizacao_atual",
                "mult": "[1..*]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "altura_edificio",
                "mult": "1",
                "tipo": "Real",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_const",
                "mult": "1",
                "tipo": "Data",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..*]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "numero_policia",
                "mult": "[0..*]",
                "tipo": "Texto",
                "d1": "x",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_condicao_const",
                "vals": [
                    {
                        "val": "1",
                        "def": "construção demolida já não subsistindo vestígios visíveis.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "construção não pode ser utilizada em condições normais, embora os seus principais elementos (paredes, telhado) ainda existam.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "construção iniciada e não concluída. este valor aplica-se unicamente à construção inicial do edifício e não a trabalhos de manutenção.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "construção em fase de projeto.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "construção parcialmente demolida.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "a construção está a ser utilizada.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_elemento_edificio_xy",
                "vals": [
                    {
                        "val": "1",
                        "def": "a geometria horizontal foi obtida a partir da combinação das geometrias das suas partes constituintes com as geometrias das partes do edifício utilizando diferentes referências de geometria horizontal.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "a geometria horizontal foi obtida por um ponto situado à entrada do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "a geometria horizontal foi obtida utilizando todo o invólucro do edifício, ou seja, a extensão máxima do edifício acima e abaixo do solo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "a geometria horizontal foi obtida utilizando a implantação do edifício, ou seja, a sua extensão ao nível do solo, incluindo os alpendres.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "a geometria horizontal foi obtida utilizando o piso mais baixo acima do solo do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6",
                        "def": "a geometria horizontal foi obtida utilizando as beiras do telhado do edifício.",
                        "d1": "",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_elemento_edificio_z",
                "vals": [
                    {
                        "val": "1",
                        "def": "altura medida ou estimada ao nível da extensão máxima do invólucro acima do solo do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "altura medida ou estimada na base da parte utilizável do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "altura medida ou estimada à entrada da edifico, geralmente na soleira da porta de entrada.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "altura medida ou estimada ao nível da cornija, em qualquer local entre os níveis mais baixo e mais alto da cornija do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "altura medida ou estimada ao nível do solo, em qualquer local entre os pontos mais baixo e mais alto do edifício no solo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6",
                        "def": "altura medida ou estimada ao nível do telhado, em qualquer local entre o nível mais baixo da beira do telhado e o topo do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7",
                        "def": "altura medida ou estimada ao nível da beira do telhado, em qualquer local entre as beiras mais baixa e mais alta do telhado do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "8",
                        "def": "a altura foi medida ou estimada ao nível da cornija mais elevada do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "9",
                        "def": "a altura foi medida ou estimada no ponto mais alto do edifício, incluindo instalações, como as chaminés e antenas.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "10",
                        "def": "a altura foi medida ou estimada ao nível da beira mais alta do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "11",
                        "def": "a altura foi medida ou estimada ao nível mais baixo da cornija do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "12",
                        "def": "a altura foi medida ou estimada ao nível do piso mais baixo acima do solo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "13",
                        "def": "a altura foi medida ou estimada ao nível da beira mais baixa do telhado do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "14",
                        "def": "a altura foi medida ou estimada ao nível do topo do edifício.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "15",
                        "def": "a altura foi medida ou estimada no ponto mais alto do edifício no solo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "16",
                        "def": "a altura foi medida ou estimada ao nível do ponto mais baixo do edifício no solo.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_forma_edificio",
                "vals": [
                    {
                        "val": "1",
                        "def": "espaço aberto onde são representadas espetáculos culturais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "estrutura artificial em forma de arco.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "estrutura com dispositivo para funcionar com base na energia hídrica e destinado à moagem de cereais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "edifício de construção ligeira, que normalmente tem um ou mais lados abertos e que é geralmente utilizado para fins de armazenamento.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "construção permanente num curso de água utilizada para reter a água ou para controlar o seu caudal.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "instalação, em parte subterrânea, destinada aos militares, ou por estes utilizada, quer para a localização de centros de comando e controlo quer para o acampamento de tropas.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7",
                        "def": "edifício de culto cristão, geralmente mais pequeno que uma igreja.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "fortificação de defesa com muralhas, torres e, por vezes, fossos, construída geralmente em local estratégico.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "conduta vertical e elevada em forma de tubo construída para exaustão de fumos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "10",
                        "def": "edificação sobrelevada, coberta mas sem paredes, localizada em largos e jardins, e onde se realizam apresentações de bandas musicais e outros espetáculos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "11",
                        "def": "estrutura normalmente de pedra ou madeira com a função de secar o milho através das fissuras laterais.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "12",
                        "def": "grande recinto destinado essencialmente à realização de competições desportivas e circundada de bancadas em anfiteatro para acomodação de público.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "13",
                        "def": "construção envidraçada ou coberta de material transparente, na qual a temperatura e humidade podem ser controladas, para cultivo e crescimento de espécies agrícolas ou plantas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "14",
                        "def": "torre ou outra construção elevada construída para apoio à navegação marítima.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "15",
                        "def": "construção arquitetónica militar projetada para defesa.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "16",
                        "def": "construção destinada ao abrigo e reparação de aeronaves e dirigíveis.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "17",
                        "def": "edifício de culto cristão.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "18",
                        "def": "reservatório de água encanada.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "19",
                        "def": "edifício de culto islâmico.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "20",
                        "def": "estrutura com dispositivo para funcionar com base na energia do vento e destinado à moagem de cereais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "21",
                        "def": "edifício grandioso e de aparência nobre.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "22",
                        "def": "construção de forma tradicionalmente circular ou em ferradura que proporciona habitat de nidificação para os pombos.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "23",
                        "def": "infraestrutura normalmente circular, com bancadas para espectadores e com uma arena interior destinada a corridas tauromáquicas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "24",
                        "def": "reservatório em forma de torre onde são armazenados materiais ou alimentos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "25",
                        "def": "edifício de culto judaico.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "26",
                        "def": "depósito geralmente para líquidos e gases comprimidos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "27",
                        "def": "abrigo de pessoas, animais, lenha, veículos ou de outros materiais, constituído por uma cobertura assente sobre pilares e por vezes tapada dos lados.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "28",
                        "def": "edifício ou estrutura de características específicas cuja principal finalidade é o culto religioso.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "29",
                        "def": "estrutura relativamente alta e estreita que pode estar isolada ou fazer parte de outra estrutura.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_utilizacao_atual",
                "vals": [
                    {
                        "val": "1",
                        "def": "edifício utilizado para fins exclusivos ou parcialmente residenciais.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "1.1",
                        "def": "edifício utilizado para fins residenciais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.2",
                        "def": "edifício ou construção de apoio à residência (inclui garagem, telheiro, arrecadação e escadas).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "edifício utilizado para fins de produção animal, caça, floresta ou pesca.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "2.1",
                        "def": "edifício utilizado para fins agrícolas, de produção animal, caça ou serviços relacionados.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "2.2",
                        "def": "edifício utilizado para atividades de silvicultura e exploração florestal.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "2.3",
                        "def": "edifício utilizado para atividades piscatórias.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "edifício utilizado para fins industriais (inclui a indústria extrativa e transformadora).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "edifício destinado ao comércio por grosso e a retalho; reparação de veículos automóveis, motociclos e de bens de uso pessoal e doméstico.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.1",
                        "def": "estabelecimento de pequena/média dimensão num ambiente em que predomina a proximidade entre o cliente e o vendedor.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4.2",
                        "def": "estabelecimento onde se reúnem vendedores e compradores no mesmo local para a troca de bens e serviços.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4.3",
                        "def": "estabelecimento que contém uma grande área comercial com um conjunto de estabelecimentos para consumo, prestação de serviços e lazer.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "edifício, ou componente de edifício, utilizado exclusivamente ou parcialmente para alojamento e/ou restauração.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "5.1",
                        "def": "edifício utilizado para alojamento (inclui hotel, turismo rural e de habitação).",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "5.2",
                        "def": "edifício de suporte ao parque de campismo e de caravanismo ou de outro local de alojamento de curta duração (inclui abrigos de montanha).",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "5.3",
                        "def": "edifício utilizado para restauração (inclui restaurante e estabelecimentos onde se e servem bebidas ou refeições ligeiras).",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "6",
                        "def": "edifício associado aos transportes.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.1",
                        "def": "edifício associado ao tráfego aéreo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.1.1",
                        "def": "edifício destinado aos serviços associados ao transporte aéreo (incluindo os serviços de check-in, embarque e desembarque, comércio, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.1.2",
                        "def": "edifício destinado ao controlo do tráfego aéreo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.2",
                        "def": "edifício associado ao tráfego ferroviário.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.2.1",
                        "def": "edifício da estação ferroviária.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.2.2",
                        "def": "edifício do apeadeiro ferroviário.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.3",
                        "def": "edifício associado ao tráfego marítimo ou fluvial.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.3.1",
                        "def": "edifício destinado aos serviços associados ao transporte marítimo ou fluvial (incluindo os serviços de check-in, embarque e desembarque, comercio, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.3.2",
                        "def": "edifício destinado ao controlo do tráfego marítimo ou fluvial.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.4",
                        "def": "edifício associado a tráfego rodoviário.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.4.1",
                        "def": "equipamento associado ao lugar de paragem do transporte rodoviário para embarque e desembarque dos passageiros.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "6.4.2",
                        "def": "edifício destinado aos serviços associados ao transporte rodoviário (incluindo os serviços de check-in, embarque e desembarque, comércio, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.4.3",
                        "def": "edifício associado a parques de estacionamento ou interfaces.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.5",
                        "def": "equipamento que conduz pessoas ou carga aos vários andares de um edifício.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.6",
                        "def": "outro edifício ou equipamento associado a tráfego aéreo, ferroviário, marítimo, fluvial, por cabo ou rodoviário.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "edifício associado à prestação de serviços.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7.1",
                        "def": "edifício afeto à administração pública e aos órgãos de soberania.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.2",
                        "def": "edifício destinado à prestação de serviços de interesse coletivo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.3",
                        "def": "edifício onde se desenvolvem atividades financeiras, informáticas, imobiliárias ou outras.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "edifício destinado a atividades associativas, recreativas, culturais ou desportivas.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "8.1",
                        "def": "organizações económicas, patronais e profissionais, sindicais, políticas ou associativas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8.2",
                        "def": "edifício onde se exerce práticas de diferentes confissões religiosas e de apoio às atividades de inumação. ",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8.3",
                        "def": "edifício onde se exerce atividades lúdicas ou culturais (cinema, vídeo, rádio, televisão, atividades artísticas e de espetáculo, agências de noticias, bibliotecas, arquivos, museus, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8.4",
                        "def": "edifício de apoio à prática de atividades desportivas (inclui apoio a estádios, pavilhões gimnodesportivos, ginásios, piscinas e edifícios de apoio a diferentes modalidades desportivas).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "edifício destinado a organizações internacionais (e.g. onu, ocde, efta, fmi), banco mundial, embaixadas e consulados.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "linha_ferrea": {
        "tema": "transportes (transporte ferroviário)",
        "geo_caract": "linha férrea",
        "def": "caracterização da linha férrea para a área geográfica representada.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "codigo_linha_ferrea",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "designacao_local": {
        "tema": "toponímia",
        "geo_caract": "designação local",
        "def": "designação de uma entidade do mundo real referenciada através de um ou mais nomes próprios.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_local_nomeado",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_local_nomeado",
                "vals": [
                    {
                        "val": "1",
                        "def": "cidade onde está situada a sede administrativa do país.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "cidade onde está situada a sede administrativa da região autónoma.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "cidade onde está situada a sede administrativa do distrito.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "lugar onde está instalada a câmara municipal e que dá o nome ao município.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "lugar onde está instalada a freguesia e que dá o nome à mesma.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "características geomorfológicas dos terrenos.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.1",
                        "def": "forma natural de relevo constituída por uma cadeia de montanhas.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "6.2",
                        "def": "zona da costa onde a terra avança pelo mar originando uma saliência natural.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.3",
                        "def": "enseada comprida e estreita na costa marítima, em que o mar invadiu os vales fluviais interiores.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.4",
                        "def": "cume de uma elevação natural com dimensões consideráveis - o mesmo que pico.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.5",
                        "def": "porção de terra cercada de água por todos os lados menos por um que a liga geralmente a uma região mais vasta, habitualmente continental.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.6",
                        "def": "reentrância da costa, geralmente entre dois cabos, de contorno aproximadamente semicircular ou estreita à entrada e mais larga no interior, de extensão considerável, menor que um golfo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.7",
                        "def": "reentrância ou recôncavo da costa marítima, que forma pequeno porto de abrigo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.8",
                        "def": "ilha pequena que fica no curso de um rio.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.9",
                        "def": "colinas de areia formadas pelo vento junto das praias.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.10",
                        "def": "terreno plano ou em declive não muito acentuado, junto ao mar subjacente a uma arriba abrupta. pode ter origem na solidificação de mantos de lavas que escorreram pelas encostas, ou no depósito de materiais provenientes do desmoronamento das arribas erodidas",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "aglomerado populacional com designação própria.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7.1",
                        "def": "aglomerado populacional com a categoria de “cidade”.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.2",
                        "def": "aglomerado populacional com a categoria de “vila”.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.3",
                        "def": "povoação de pequenas dimensões e densidade populacional reduzida (aldeia, lugar, casal, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "nome de local que se destaca e constitui uma referência conhecida da população (pinhal, mata, mouchão, lombo, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "área relevante para a conservação da natureza e da biodiversidade e que é objeto de regulamentação específica (inclui o parque natural, os parques e reservas naturais e as paisagens protegidas).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "10",
                        "def": "faixa arenosa no litoral marítimo ou numa parte de uma margem ribeirinha.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "11",
                        "def": "grande massa de água salgada na superfície da terra.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "12",
                        "def": "grupo de ilhas mais ou menos próximas entre si.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "13",
                        "def": "extensão de terra cercada de água por todos os lados.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "14",
                        "def": "ilha de pequenas dimensões, normalmente não habitada.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "15",
                        "def": "local não enquadrável em qualquer um dos outros valores. ",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "linha_de_quebra": {
        "tema": "altimetria",
        "geo_caract": "linha de quebra",
        "def": "linha crítica que descreve a forma de uma superfície altimétrica e indica uma descontinuidade no declive do terreno.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_classifica",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_natureza_linha",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "artificial",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_classifica",
                "vals": [
                    {
                        "val": "1",
                        "def": "linha definidora do bordo inferior do terreno inclinado.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "linha ou local onde o gradiente altimétrico se altera.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "linha de rutura que representa uma orientação local em que a superfície altimétrica objeto de descrição apresenta o maior declive.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "linha de reunião de duas vertentes do terreno com concavidade voltada para cima (pontos de menor cota).",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "linha de reunião de duas vertentes do terreno com concavidade voltada para baixo (pontos de maior cota). linha de festo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6",
                        "def": "linha definidora do bordo superior de terreno inclinado.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_natureza_linha",
                "vals": [
                    {
                        "val": "1",
                        "def": "linha definidora do bordo de terreno de elevada inclinação, quase a pique.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "linha delimitadora da zona superior de aterro (zona onde se depositaram terras para definir uma sobrelevação) ou da zona superior do desaterro (zonas de onde se retiraram terras para gerar uma depressão).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "limite superior da porção de terreno determinada natural ou artificialmente em encostas, relativamente estreita, destinando-se a cultivo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "limite superior de pequena elevação no terreno, normalmente artificial para separar várias zonas de água, nomeadamente em arrozais e outras culturas semelhantes.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "ponto_cotado": {
        "tema": "altimetria",
        "geo_caract": "ponto cotado",
        "def": "projeção ortogonal de um ponto do terreno no plano.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_classifica_las",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_classifica_l_a_s",
                "vals": [
                    {
                        "val": "1",
                        "def": "ponto cotado no terreno.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "ponto cotado no edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.1",
                        "def": "ponto cotado na soleira do edifício.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.2",
                        "def": "ponto cotado no remate inferior do telhado.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.3",
                        "def": "ponto cotado em cumeeira ou na zona mais elevada do edifício.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "area_agricola_florestal_mato": {
        "tema": "ocupação do solo",
        "geo_caract": "área agrícola, florestal ou mato",
        "def": "áreas naturais de ocupação do solo.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_areas_agricolas_florestais_matos",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_areas_agricolas_florestais_matos",
                "vals": [
                    {
                        "val": "1",
                        "def": "áreas utilizadas para agricultura, constituídas por terras aráveis e culturas permanentes.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "1.1",
                        "def": "culturas agrícolas temporárias irrigadas e não irrigadas.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "1.2",
                        "def": "área de uso agrícola preparada para o cultivo do arroz.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "1.3",
                        "def": "área com plantações de vinha.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "1.4",
                        "def": "área com árvores ou arbustos de uma ou várias espécies destinados à produção de fruto.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "1.5",
                        "def": "área de plantação de oliveiras (olea europea var. europea) para produção de azeitona.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "área ocupada com vegetação essencialmente do tipo herbácea.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.1",
                        "def": "área ocupada com vegetação predominantemente herbácea semeada, não incluída em sistemas de rotação de culturas, podendo ou não ter pastoreio in situ, e que acessoriamente pode ou não ser cortada em determinados períodos do ano.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2.2",
                        "def": "área ocupada com vegetação predominantemente herbácea espontânea onde não existe qualquer intervenção do homem.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "área de terrenos dedicados aos usos florestais e agrícolas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "área de terreno coberto de árvores e de outras formações vegetais.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4.1",
                        "def": "floresta de espécies arbóreas angiospérmicas.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4.1.1",
                        "def": "área ocupada por conjuntos de árvores da espécie sobreiro (quercus suber).",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.1.2",
                        "def": "área ocupada por conjuntos de árvores da espécie azinheira (quercus rotundifolia).",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.1.3",
                        "def": "área ocupada por conjuntos de árvores florestais das espécies carvalho-negral (quercus pyrenaica), carvalho-alvarinho (quercus robur), carvalho-português (quercus faginea), ou de outros carvalhos.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.1.4",
                        "def": "área ocupada por conjuntos de árvores da espécie castanheiro (castanea sativa) resultantes de regeneração natural, sementeira ou plantação.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.1.5",
                        "def": "área ocupada por conjuntos de árvores da espécie eucalipto (eucalyptus spp.) resultantes de regeneração natural, sementeira ou plantação.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.1.6",
                        "def": "área ocupada por conjuntos de árvores de espécie de carácter invasor.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.1.7",
                        "def": "área ocupada por conjuntos de árvores em que se verifica a dominância numa espécie de outras folhosas não discriminadas nas restantes classes de folhosas (inclui floresta de nogueira (juglans regia) desde que explorada para a produção de madeira).",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.2",
                        "def": "floresta de espécies arbóreas gimnospérmicas.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4.2.1",
                        "def": "área ocupada por conjuntos de árvores da espécie pinheiro manso (pinus pinea) resultantes de regeneração natural, sementeira ou plantação.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.2.2",
                        "def": "área ocupada por conjuntos de árvores da espécie pinheiro bravo (pinus pinaster) resultantes de regeneração natural, sementeira ou plantação.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "4.2.3",
                        "def": "área ocupada por conjuntos de árvores em que se verifica a dominância numa espécie de outras resinosas não discriminadas nas restantes classes de resinosas.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "área natural de vegetação espontânea (urzes, silvas, giestas, tojos, zambujeiro).",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "no_trans_rodov": {
        "tema": "transportes (transporte rodoviário)",
        "geo_caract": "nó de transporte rodoviário",
        "def": "ponto utilizado para representar a conectividade entre segmentos da via rodoviária, estabelecer a relação com outras vias de comunicação e com as infraestruturas associadas à via rodoviária.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_no_trans_rodov",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_no_trans_rodov",
                "vals": [
                    {
                        "val": "1",
                        "def": "nó que identifica a interseção ao mesmo nível de 3 ou mais segmentos da via rodoviária (incluindo cruzamentos, entroncamentos e rotundas).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "nó que identifica a interseção ao mesmo nível do segmento da via rodoviária com o segmento da via ferroviária.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "nó que representa um ponto que une dois segmentos da via rodoviária com características distintas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "nó que identifica o fim do segmento da via rodoviária (não existe conetividade com outro segmento).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "nó que identifica a presença de uma infraestrutura de transporte rodoviário.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "infra_trans_rodov": {
        "tema": "transportes (transporte rodoviário)",
        "geo_caract": "infraestrutura de transporte rodoviário",
        "def": "ponto que caracteriza a infraestrutura de transporte rodoviário.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_infra_trans_rodov",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_servico",
                "mult": "[1..*]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_infra_trans_rodov",
                "vals": [
                    {
                        "val": "1",
                        "def": "local de paragem de uma linha de transporte rodoviário.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "instalação onde funciona o término de uma linha de transporte rodoviário.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "local destinado ao estacionamento de veículos.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "área delimitada ao ar livre para guardar temporariamente veículos, em geral mediante pagamento.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "zona da via rodoviária destinada à cobrança manual ou automática de taxas de portagem (cabines de portagens tradicionais, ou pórticos - exclusivamente eletrónicas).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "o espaço marginal à via rodoviária, podendo ser provido de sombreamento, iluminação, água potável, mesas e bancos ao ar livre, estacionamento para veículos ligeiros e pesados, instalações sanitárias, recolha de lixo e outros equipamentos de apoio aos utilizadores.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "instalação associada a uma via rodoviária e que inclui equipamentos e meios destinados ao fornecimento de combustível e energia e eventual apoio aos utilizadores e aos veículos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "instalação marginal à via rodoviária contendo equipamentos destinados ao fornecimento de combustíveis e energia.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_servico",
                "vals": [
                    {
                        "val": "1",
                        "def": "combustível disponível (gasolina, gasóleo, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "carregamento elétrico disponível.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "loja de conveniência disponível.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "restauração disponível.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "estacionamento para veículos ligeiros disponível.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "estacionamento para veículos pesados disponível.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "estacionamento para caravanas disponível.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "8",
                        "def": "apoio automóvel disponível (inclui oficinas, serviços de lavagem, etc.).",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "9",
                        "def": "parque infantil disponível.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "10",
                        "def": "instalações sanitárias disponíveis.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "11",
                        "def": "duche disponível.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "12",
                        "def": "área de piquenique disponível.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "995",
                        "def": "",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "area_infra_trans_aereo": {
        "tema": "transportes (transporte aéreo)",
        "geo_caract": "área da infraestrutura de transporte aéreo",
        "def": "área que dispõe de instalações, equipamentos e serviços destinados ao tráfego aéreo.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_area_infra_trans_aereo",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_area_infra_trans_aereo",
                "vals": [
                    {
                        "val": "1",
                        "def": "perímetro envolvente ao aeródromo, heliporto, aeródromo com heliporto ou local de aterragem.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "área preparada para a aterragem e descolagem de aeronaves.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "via destinada à circulação de aeronaves e a estabelecer uma ligação entre partes do aeródromo (taxiway).",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "área destinada a receber aeronaves para fins de embarque ou desembarque de passageiros, correio ou carga e para abastecimento de combustível, estacionamento ou manutenção.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "curso_de_agua_eixo": {
        "tema": "hidrografia",
        "geo_caract": "curso de água - eixo",
        "def": "eixo das águas de um rio, ribeira ou outra corrente natural ou artificial.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_curso_de_agua",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_persistencia_hidrologica",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "valor_posicao_vertical",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "comprimento",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "delimitacao_conhecida",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "ficticio",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "largura",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "ordem_hidrologica",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "origem_natural",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_curso_de_agua",
                "vals": [
                    {
                        "val": "1",
                        "def": "troço ou percurso total de curso de água que permite a navegação.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "troço ou percurso total de curso de água que não permite a navegação nem a flutuabilidade.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "pequeno curso de água de dimensões e caudal, permanente ou temporário, inferiores aos de um rio.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "linha correspondente ao talvegue definido por duas ou mais vertentes, onde corre água com caudal permanente ou temporário.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "passagem artificial de águas por vezes em forma de calha, para conduzir águas de caudal mais ou menos constante, para usos geralmente industriais ou agrícolas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "fosso longo, mais ou menos largo, para conduzir águas de caudal mais ou menos constante, para usos geralmente industriais ou agrícolas.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_persistencia_hidrologica",
                "vals": [
                    {
                        "val": "1",
                        "def": "cheio e/ou a correr raramente, de um modo geral, unicamente durante e /ou logo após forte precipitação.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "cheio e/ou a correr durante e logo após precipitações.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "cheio e/ou a correr durante uma parte do ano.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "cheio e/ou a correr continuamente ao longo do ano.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_posicao_vertical",
                "vals": [
                    {
                        "val": "1",
                        "def": "a entidade geográfica está acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "0",
                        "def": "a entidade geográfica está assente diretamente no solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-1",
                        "def": "a entidade geográfica está assente abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "seg_via_ferrea": {
        "tema": "transportes (transporte ferroviário)",
        "geo_caract": "segmento da via-férrea",
        "def": "eixo da via-férrea.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_categoria_bitola",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_estado_linha_ferrea",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_posicao_vertical_transportes",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_linha_ferrea",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_troco_via_ferrea",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_via_ferrea",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "eletrific",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "gestao",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "jurisdicao",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "velocidade_max",
                "mult": "1",
                "tipo": "Inteiro",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_categoria_bitola",
                "vals": [
                    {
                        "val": "1",
                        "def": "a bitola nominal da via é de 1668 milímetros.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "a bitola nominal da via é de 1435 milímetros.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "a bitola nominal da via é de 1000 milímetros.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "995",
                        "def": "",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_estado_linha_ferrea",
                "vals": [
                    {
                        "val": "1",
                        "def": "a instalação já não é utilizada e foi ou está a ser desmantelada.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "a instalação encontra-se em fase de construção e ainda não está concluída. este valor aplica-se unicamente à construção inicial da instalação e não a trabalhos de manutenção.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "a instalação já não é utilizada, mas não está a ser ou não foi desmantelada. a linha pode estar desafetada (fora de serviço) ou fechada ao tráfego (linha não utilizada em condições normais).",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "a instalação encontra-se em fase de projeto. os trabalhos de construção ainda não tiveram início.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "a linha férrea está em exploração (aberta ao tráfego).",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_posicao_vertical_transportes",
                "vals": [
                    {
                        "val": "3",
                        "def": "a entidade geográfica está assente no terceiro ou superior nível acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "a entidade geográfica está assente no segundo nível acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1",
                        "def": "a entidade geográfica está assente no primeiro nível acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "0",
                        "def": "a entidade geográfica está assente diretamente no solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-1",
                        "def": "a entidade geográfica está assente no primeiro nível abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-2",
                        "def": "a entidade geográfica está assente no segundo nível abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-3",
                        "def": "a entidade geográfica está assente no terceiro ou inferior nível abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_linha_ferrea",
                "vals": [
                    {
                        "val": "1",
                        "def": "transporte ferroviário que permite a circulação dos veículos a grandes desníveis, composto por uma via-férrea com ferrovia de cremalheira (situada geralmente entre os carris de rolamento) e veículos equipados com uma ou várias rodas dentadas ou pinhões que engrenam nessa cremalheira.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "transporte ferroviário que compreende um cabo ligado a um veículo que circula em carris permitindo a subida e a descida de um declive muito íngreme. se possível, os veículos que fazem os percursos ascendentes e descendente contrabalançam-se.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "transporte ferroviário assente num único carril, com a função de guiamento, que o suporta por meio de um mecanismo de levitação magnética.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "sistema de transporte ferroviário urbano utilizado em zonas urbanas e suburbanas, que circula numa via independente dos outros sistemas de transporte acionado eletricamente e cujo percurso é subterrâneo e à superfície.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "transporte ferroviário assente num único carril, com função simultânea de apoio e guiamento.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "transporte ferroviário assente num único carril, com função de apoio e guiamento, no qual está suspenso um veículo que se desloca ao longo deste.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "transporte ferroviário que geralmente consiste em dois carris paralelos sobre os quais um veículo ou uma máquina movidos a energia aciona uma série de veículos atrelados permitindo a sua circulação ao longo da via-férrea a fim de transportar pessoas ou mercadorias de um destino para outro.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "sistema de transporte ferroviário, utilizado em zonas urbanas, que partilha o espaço rodoviário com veículo e peões.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_troco_via_ferrea",
                "vals": [
                    {
                        "val": "1",
                        "def": "via ferroviária cujo perfil transversal apresenta uma só via que pode ser percorrida nos dois sentidos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "via ferroviária cujo perfil transversal apresenta duas vias em que, normalmente, há um só sentido de circulação para cada via.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "via ferroviária cujo perfil transversal apresenta mais do que duas vias em que, normalmente, há um só sentido de circulação para cada via.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_via_ferrea",
                "vals": [
                    {
                        "val": "1",
                        "def": "via entre estações que constitui a via-férrea principal.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "via de uma estação que constitui a via-férrea principal.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "instalação de via numa estação ou depósito para estacionamento e, se necessário, para limpeza das composições.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "linha destinada ao resguardo de um comboio, permitindo a sua ultrapassagem, em segurança, por outro considerado prioritário.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "via inserida nas instalações de uma empresa.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "elem_assoc_telecomunicacoes": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "elemento associado de telecomunicações",
        "def": "infraestrutura associada ao funcionamento da rede de telecomunicações.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_elemento_associado_telecomunicacoes",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_elemento_associado_telecomunicacoes",
                "vals": [
                    {
                        "val": "1",
                        "def": "haste vertical colocada no solo ou fixada em alvenaria, de suporte das linhas telefónicas.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "pequena construção ou compartimento isolado para proteção do utente no uso de telefone público.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "estrutura, sobre o solo ou no topo dos edifícios, destinada às comunicações por meio da captação e irradiação de ondas eletromagnéticas ou radielétricas.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "curso_de_agua_area": {
        "tema": "hidrografia",
        "geo_caract": "curso de água - área",
        "def": "área limite das águas de um rio, ribeira ou outra corrente natural ou artificial.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "delimitacao_conhecida",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "margem": {
        "tema": "hidrografia",
        "geo_caract": "margem",
        "def": "faixa de terra em contacto com qualquer massa de água.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_margem",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_margem",
                "vals": [
                    {
                        "val": "1",
                        "def": "margem constituída por pedras grandes erodidas pela água ou pela ação do tempo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "margem constituída por terra dura e tenaz de grão fino, constituída sobretudo por alumino-silicatos.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "margem constituída por pedra miúda misturada geralmente com areia grossa.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "margem constituída por solo macio e húmido, areia, pó ou qualquer outro material terroso.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "margem constituída por rochas de qualquer tamanho.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "margem constituída por fragmentos pequenos e erodidos de rochas (sobretudo silicatos), mais finas que o cascalho e maiores que o grão de lodo solto.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "margem constituída por calhaus pequenos, soltos e arredondados pela ação da água.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "8",
                        "def": "margem constituída por partes de rocha ou de uma substância mineral usualmente artificial e com um propósito especial.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "995",
                        "def": "",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "area_infra_trans_ferrov": {
        "tema": "transportes (transporte ferroviário)",
        "geo_caract": "área da infraestrutura de transporte ferroviário",
        "def": "área que dispõe de instalações, equipamentos e serviços destinados ao tráfego ferroviário.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "inst_gestao_ambiental": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "instalação de gestão ambiental",
        "def": "infraestrutura de gestão de resíduos ou águas residuais.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "valor_instalacao_de_gestao_ambiental",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_instalacao_de_gestao_ambiental",
                "vals": [
                    {
                        "val": "1",
                        "def": "instalação para depósito e tratamento de resíduos sólidos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "instalação para depósito tratamento de resíduos líquidos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "instalação para depósito tratamento de resíduos industriais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "instalação para depósito tratamento de resíduos tóxicos.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "via_rodov_limite": {
        "tema": "transportes (transporte rodoviário)",
        "geo_caract": "via rodoviária - limite",
        "def": "linha que materializa os limites da via rodoviária.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "X"
            },
            {
                "nome": "valor_tipo_limite",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_limite",
                "vals": [
                    {
                        "val": "1",
                        "def": "linha que materializa o limite da via rodoviária pelo limite exterior da zona utilizada pelo tráfego (sem berma direita), ou seja, pelo limite geométrico exterior da faixa de rodagem (constituída unicamente pela largura das vias de trânsito), caso exista sinalização horizontal (guia). caso esta sinalização não exista, também não existirá berma direita e como tal o limite da via rodoviária coincide com o limite exterior da área pavimentada ou não pavimentada.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "linha que delimita a área destinada a separar o tráfego do mesmo sentido ou de sentidos opostos, incluindo, caso existam, as bermas esquerdas. aplica-se em: rotunda, separador central (distância entre os bordos interiores das faixas unidirecionais) ou lateral, ilhéu direcional ou dispositivo semelhante.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "linha que materializa o limite da via rodoviária pelo limite exterior da berma direita pavimentada, caso exista sinalização horizontal (guia). caso esta sinalização não exista também não existirá berma direita e como tal o limite da via rodoviária não é representável.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "cabo_electrico": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "cabo elétrico",
        "def": "linhas elétricas aéreas ou subterrâneas destinadas ao transporte ou distribuição de energia elétrica.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_designacao_tensao",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_posicao_vertical",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "tensao_nominal",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_designacao_tensao",
                "vals": [
                    {
                        "val": "1",
                        "def": "tensão entre fases cujo valor eficaz é superior a 110 kv.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "tensão entre fases cujo valor eficaz é superior a 45 kv e igual ou inferior a 110 kv.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "tensão entre fases cujo valor eficaz é superior a 1 kv e igual ou inferior a 45 kv.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "tensão entre fases cujo valor eficaz é igual ou inferior a 1 kv.",
                        "d1": "x",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_posicao_vertical",
                "vals": [
                    {
                        "val": "1",
                        "def": "a entidade geográfica está acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "0",
                        "def": "a entidade geográfica está assente diretamente no solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-1",
                        "def": "a entidade geográfica está assente abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "via_rodov": {
        "tema": "transportes (transporte rodoviário)",
        "geo_caract": "via rodoviária",
        "def": "caracterização das vias rodoviárias para a área geográfica representada.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "codigo_via_rodov",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_cat",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fonte_aquisicao_dados",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome_alternativo",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "tipo_via_rodov_abv",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "tipo_via_rodov_c",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "tipo_via_rodov_d",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "infra_trans_via_navegavel": {
        "tema": "transportes (transporte por via navegável)",
        "geo_caract": "infraestrutura de transporte por via navegável",
        "def": "ponto que caracteriza a infraestrutura de transporte por via navegável.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_infra_trans_via_navegavel",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "codigo_via_navegavel",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_infra_trans_via_navegavel",
                "vals": [
                    {
                        "val": "1",
                        "def": "local na costa, rio ou lago que dispõe de um conjunto de infraestruturas e de serviços, incluindo os de logística, de suporte ao comercio marítimo, pesca, transporte de passageiros, náutica de recreio, maritímo-turistíca, desportos náuticos, e, ainda de bases navais militares.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "estrutura junto da qual os navios procedem à acostagem a fim de efetuarem operações de embarque e desembarque de passageiros ou mercadorias.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "local dotado de cais acostáveis para navios e embarcações (inclui marinas).",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "concelho": {
        "tema": "unidades administrativas",
        "geo_caract": "concelho",
        "def": "divisão administrativa estabelecida de acordo com a constituição portuguesa. ",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (multipolígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "dico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_publicacao",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "conduta_de_agua": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "conduta de água",
        "def": "conduta utilizada para transportar água de um local para outro.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_conduta_agua",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_posicao_vertical",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "diametro",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_conduta_agua",
                "vals": [
                    {
                        "val": "1",
                        "def": "construção de pedra ou destinada ao transporte de água, incluindo aqueduto sobre arcadas ou pilares.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "tubo condutor de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "fosso estreito artificial que se destina a coletar e a conduzir as águas superficiais para fora da via rodoviária.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_posicao_vertical",
                "vals": [
                    {
                        "val": "1",
                        "def": "a entidade geográfica está acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "0",
                        "def": "a entidade geográfica está assente diretamente no solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-1",
                        "def": "a entidade geográfica está assente abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "freguesia": {
        "tema": "unidades administrativas",
        "geo_caract": "freguesia",
        "def": "classificação de mais baixa hierarquia dentro dos níveis possíveis no quadro de divisão administrativa estabelecida de acordo com a constituição portuguesa.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (multipolígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_publicacao",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "dicofre",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "fronteira": {
        "tema": "unidades administrativas",
        "geo_caract": "fronteira",
        "def": "linha que representa a fronteira de portugal.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_estado_fronteira",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_publicacao",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_estado_fronteira",
                "vals": [
                    {
                        "val": "1",
                        "def": "troço obtido a partir de procedimentos realizados para o efeito.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "troço não acordado entre portugal e espanha.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "area_infra_trans_via_navegavel": {
        "tema": "transportes (transporte por via navegável)",
        "geo_caract": "área da infraestrutura de transporte por via navegável",
        "def": "área que dispõe de instalações, equipamentos e serviços destinados ao tráfego por via navegável.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_area_infra_trans_via_navegavel",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_area_infra_trans_via_navegavel",
                "vals": [
                    {
                        "val": "1",
                        "def": "área localizada na costa, rio ou lago que dispõe de um conjunto de infraestruturas e de serviços , incluindo os de logística, de suporte ao comercio marítimo, pesca, transporte de passageiros, náutica de recreio, marítimo-turística, desportos náuticos, e, ainda de bases navais militares.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "área da estrutura junto da qual os navios procedem à acostagem a fim de efetuarem operações de embarque e desembarque de passageiros ou mercadorias.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "área dotada de cais acostáveis para navios e embarcações (inclui as marinas).",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "elem_assoc_agua": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "elemento associado de água",
        "def": "infraestrutura associada ao tratamento, transporte ou distribuição de água.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_elemento_associado_agua",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_elemento_associado_agua",
                "vals": [
                    {
                        "val": "1",
                        "def": "equipamento normalmente instalado na rede pública de abastecimento de água, também designado por marco de água, que permite a ligação de equipamentos de luta contra incêndios e o reabastecimento dos veículos dos bombeiros.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "instalação dotada de equipamentos de bombagem destinados a receber e distribuir água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "instalação destinada ao tratamento de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "construção feita geralmente de pedra provida de uma ou mais bicas ou torneiras por onde corre água (inclui chafariz, fontanário ou bica).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "perfuração vertical, geralmente cilíndrica, normalmente revestida a alvenaria ou pedras sobrepostas, e orientada para a deteção e exploração do lençol de água mais perto da superfície do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "perfuração destinada à captação de água a grande profundidade.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7",
                        "def": "construção, elevada ou à superfície, utilizada como depósito de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "engenho ou aparelho para retirar água de poços ou cisternas, constituído por uma roda com pequenos reservatórios ou alcatruzes.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "infraestrutura destinada à captação de água de um curso de água ou de uma “água lêntica”.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "10",
                        "def": "compartimento que permite aceder à conduta ou à tubagem de rede de águas pluviais ou de esgotos.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "11",
                        "def": "escoadouro nas ruas para as águas, geralmente da chuva, que pode estar associado a um lancil ou a uma valeta, cuja entrada de caudal é feita superiormente, através de grade.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "12",
                        "def": "escoadouro nas ruas para as águas, geralmente da chuva, entrarem por uma abertura lateral, localizada na face vertical do lancil sob o passeio.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "area_infra_trans_rodov": {
        "tema": "transportes (transporte rodoviário)",
        "geo_caract": "área da infraestrutura de transporte rodoviário",
        "def": "área que dispõe de instalações, equipamentos e serviços destinados ao tráfego rodoviário.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "seg_via_cabo": {
        "tema": "transportes (transporte por cabo)",
        "geo_caract": "segmento da via por cabo",
        "def": "linha de transporte de pessoas ou de materiais por meio de cabo suspenso.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_via_cabo",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_via_cabo",
                "vals": [
                    {
                        "val": "1",
                        "def": "transporte por cabo cujos veículos são compostos por uma cabina suspensa utilizada para transportar, de um local para outro, grupos de pessoas e/ou mercadorias que se encontram no interior das mesmas.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "transporte por cabo cujos veículos são compostos por cadeiras suspensas utilizadas para transportar pessoas ou grupos de pessoas de um local para outro através de um cabo de aço ou de uma corda presa com uma alça em dois pontos.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "sistema de transporte por cabo utilizado para transportar esquiadores nas subidas.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "agua_lentica": {
        "tema": "hidrografia",
        "geo_caract": "água lêntica",
        "def": "massa de água totalmente rodeada por terra ou localizada junto à costa.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_agua_lentica",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_persistencia_hidrologica",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "cota_pleno_armazenamento",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_fonte_dados",
                "mult": "[0..1]",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "mare",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "origem_natural",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "profundidade_media",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_agua_lentica",
                "vals": [
                    {
                        "val": "1",
                        "def": "extensão de água, normalmente de pouca profundidade, rodeada de terra por todos os lados, que ocupa permanentemente uma depressão do terreno.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "extensão de água junto à costa resultante da ação do mar ou lago artificial criado por barragem ou represa construída num curso de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "escavação em terreno feita com o objetivo de captação e gestão de águas para fins agrícolas.",
                        "d1": "x",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_persistencia_hidrologica",
                "vals": [
                    {
                        "val": "1",
                        "def": "cheio e/ou a correr raramente, de um modo geral, unicamente durante e /ou logo após forte precipitação.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "cheio e/ou a correr durante e logo após precipitações.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "cheio e/ou a correr durante uma parte do ano.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "cheio e/ou a correr continuamente ao longo do ano.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "areas_artificializadas": {
        "tema": "ocupação do solo",
        "geo_caract": "área artificializada",
        "def": "área artificializada de ocupação do solo.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_areas_artificializadas",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_areas_artificializadas",
                "vals": [
                    {
                        "val": "1",
                        "def": "área associada aos equipamentos de saúde.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "área associada aos equipamentos de educação.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "área associada à atividade industrial.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "área associada à atividade comercial e de carácter geral (inclui os empreendimentos turísticos, as grandes superfícies comerciais e outros equipamentos similares).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "área de deposição e tratamento de resíduos urbanos ou industriais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "área em construção (inclui escavações, estaleiros de obra, etc.) e áreas abandonadas inseridas num contexto urbano.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "área ocupada por instalações desportivas (estádios de futebol e infraestruturas anexas, estádios de hóquei, piscinas e campos de ténis, pistas de ciclismo, hipódromos, pistas de atletismo, campos de tiro, etc.) e por outros equipamentos de lazer (parques e jardins, áreas verdes, campos de golfe, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "área ocupada por estruturas de apoio ao campismo ou ao caravanismo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "terreno ou local, geralmente murado, destinado à sepultura dos defuntos (exemplo: cemitérios).",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "obra_arte": {
        "tema": "transportes (obra de arte)",
        "geo_caract": "obra de arte",
        "def": "estrutura destinada à transposição de linhas de água, vales ou vias destinadas ao tráfego rodoviário, ferroviário, pedonal ou fauna.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "Data/hora",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "Data/hora",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_obra_arte",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_obra_arte",
                "vals": [
                    {
                        "val": "1",
                        "def": "construção que liga dois pontos separados por um curso de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "construção sobre um vale ou uma via rodoviária para estabelecer a comunicação entre as duas vertentes.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "construção destinada a dar passagem a uma via rodoviária sobre uma via-férrea ou uma via rodoviária de maior importância.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "construção destinada a dar passagem a uma via rodoviária sob um via-férrea ou uma via rodoviária de maior importância, incluindo passagens agrícolas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "passagem abobadada por baixo de monte, rio, canal ou mar.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "construção destinada à passagem de água sob uma via-férrea ou via rodoviária.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "construção destinada à passagem de peões, incluindo as pontes pedonais. ",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "construção relativa à sapata em altura destinada a apoio a uma estrutura.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "infra_trans_ferrov": {
        "tema": "transportes (transporte ferroviário)",
        "geo_caract": "infraestrutura de transporte ferroviário",
        "def": "ponto que caracteriza a infraestrutura de transporte ferroviário.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_infra_trans_ferrov",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_uso_infra_trans_ferrov",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "codigo_infra_trans_ferrov",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nplataformas",
                "mult": "[0..1]",
                "tipo": "Inteiro",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_infra_trans_ferrov",
                "vals": [
                    {
                        "val": "1",
                        "def": "local de paragem de um meio de transporte de carga ou passageiros, para embarque, desembarque, etc. inclui as estações de metro.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "lugar onde não há estação e em que o meio de transporte serve apenas para deixar ou receber passageiros.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_uso_infra_trans_ferrov",
                "vals": [
                    {
                        "val": "1",
                        "def": "o uso da ferrovia é exclusivamente para o transporte de passageiros.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "o uso da ferrovia é exclusivamente para operações de carga de mercadorias.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "o uso da ferrovia é usado para transportar passageiros e mercadorias.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "oleoduto_gasoduto_subtancias_quimicas": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "oleoduto, gasoduto ou substâncias químicas",
        "def": "tubagem utilizada para transportar petróleo, gás ou produtos químicos de um local para outro.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_gasoduto_oleoduto_sub_quimicas",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_posicao_vertical",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "diametro",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_gasoduto_oleoduto_sub_quimicas",
                "vals": [
                    {
                        "val": "1",
                        "def": "conduta de transporte de gás natural em estado gasoso e com diferentes pressões de serviço.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.1",
                        "def": "conduta de transporte de gás natural em estado gasoso e com pressão de serviço superior a 20 bar.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "1.2",
                        "def": "conduta de transporte de gás natural em estado gasoso e com pressão de serviço igual ou inferior a 20 bar e superior a 4 bar.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "1.3",
                        "def": "conduta de transporte de gás natural em estado gasoso e com pressão de serviço igual ou inferior a 4 bar.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "tubagem para transportar ou conduzir petróleo ou seus derivados.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "tubagem para transportar produtos químicos.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_posicao_vertical",
                "vals": [
                    {
                        "val": "1",
                        "def": "a entidade geográfica está acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "0",
                        "def": "a entidade geográfica está assente diretamente no solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-1",
                        "def": "a entidade geográfica está assente abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "queda_de_agua": {
        "tema": "hidrografia",
        "geo_caract": "queda de água",
        "def": "local onde ocorre a queda de uma massa de água devido a um desnível no curso de água.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "altura",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "infra_trans_aereo": {
        "tema": "transportes (transporte aéreo)",
        "geo_caract": "infraestrutura de transporte aéreo",
        "def": "ponto que caracteriza a infraestrutura de transporte aéreo.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_categoria_infra_trans_aereo",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_restricao_infra_trans_aereo",
                "mult": "[0..1]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_infra_trans_aereo",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "codigo_i_a_t_a",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "codigo_i_c_a_o",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_categoria_infra_trans_aereo",
                "vals": [
                    {
                        "val": "1",
                        "def": "aeródromo que presta serviços de transporte aéreo internacionais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "aeródromo que presta serviços de transporte aéreo nacionais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "aeródromo que presta serviços de transporte aéreo regionais.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_restricao_infra_trans_aereo",
                "vals": [
                    {
                        "val": "1",
                        "def": "infraestrutura destinada exclusivamente para fins militares.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "a infraestrutura está sujeita a restrições temporais.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "995",
                        "def": "",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_infra_trans_aereo",
                "vals": [
                    {
                        "val": "1",
                        "def": "área definida em terra ou na água, incluindo edifícios, instalações e equipamentos, destinada a ser usada no todo ou em parte para a chegada, partida e movimento de aeronaves.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "aeródromo ou área definida numa estrutura com vista a ser usada, no todo ou em parte, para a chegada, partida e movimentos à superfície de helicópteros e respetivos serviços de apoio.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "aeródromo com heliporto. ",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "área sem infraestruturas de apoio associadas que permite a descolagem e a aterragem de aeronaves (aviões, helicópteros, etc.).",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "inst_producao": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "instalação de produção",
        "def": "instalação técnica para fins específicos industriais ou de produção, compreendendo todos os equipamentos.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "valor_instalacao_producao",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "descricao_da_funcao",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_instalacao_producao",
                "vals": [
                    {
                        "val": "1",
                        "def": "instalação ou edifício onde é exercida uma atividade pecuária",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "1.1",
                        "def": "instalação ou edifício onde é exercida uma atividade pecuária relacionada com exploração, reprodução e recriação de gado bovino.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "1.2",
                        "def": "instalação ou edifício onde é exercida uma atividade pecuária relacionada com exploração, reprodução e recriação de gado suíno.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "1.3",
                        "def": "instalação ou edifício onde é exercida uma atividade pecuária relacionada com exploração, reprodução e recriação de aves.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "1.4",
                        "def": "edifício onde se abatem animais cuja carne é para consumo público.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "instalação ou edifício onde é exercida a atividade de vinificação relacionada com a transformação da uva em vinho e a conservação, acondicionamento e comercialização de vinhos.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "instalação ou edifício onde é exercida a atividade relacionada com a transformação da azeitona em azeite.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "instalação ou edifício onde são realizadas atividades relacionadas de minerais ou rochas.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4.1",
                        "def": "área a céu aberto onde se extrai rochas ou massas minerais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4.2",
                        "def": "área que delimita um conjunto de escavações e instalações destinadas à exploração de minérios ou outros produtos existentes no subsolo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4.3",
                        "def": "extração de materiais em locais fluviais ou marítimos.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4.4",
                        "def": "terreno plano à beira do mar, de lago salgado ou de rio de água salgada, dividido em reservatórios nos quais se deposita o sal.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "edifício de estabelecimento industrial onde se procede à transformação ou conservação de matérias-primas e onde se obtêm ou fabricam produtos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "edifício onde se fabricam materiais explosivos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "lugar onde se exerce algum ofício.",
                        "d1": "",
                        "d2": "x"
                    },
                    {
                        "val": "7.1",
                        "def": "edifício onde se encontram equipamentos ou máquinas para fabrico e reparação de objetos metálicos ou não, aparelhos, instrumentos, etc., incluindo carpintarias e serrações.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "7.2",
                        "def": "edifício ou área onde se fabricam materiais pirotécnicos.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "7.3",
                        "def": "edifício onde se encontram equipamentos ou máquinas para reparação e substituição de peças de veículos automóveis ou outros ou onde se lavam esses veículos.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "8",
                        "def": "edifícios associados a áreas onde se constroem ou reparam embarcações.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "edifício onde se arrecadam diversos tipos de produtos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "10",
                        "def": "edifício de apoio ao funcionamento das antenas destinadas às comunicações por meio da captação e irradiação de ondas eletromagnéticas ou radielétricas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "11",
                        "def": "área com as instalações e exploração de culturas em águas marinhas, águas superficiais na proximidade da foz dos rios, que têm um caráter parcialmente salgado.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "12",
                        "def": "área onde se depositam materiais de ferro velho e onde se compram e vendem esses materiais.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "curva_de_nivel": {
        "tema": "altimetria",
        "geo_caract": "curva de nível",
        "def": "linha imaginária que resulta da interseção de uma superfície de nível com o terreno na qual todos os pontos têm igual valor de elevação, referida esta a um determinado datum altimétrico.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_curva",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_curva",
                "vals": [
                    {
                        "val": "1",
                        "def": "curva cujo valor da elevação corresponde a 5 vezes o valor da equidistância natural.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "curva cujo valor da elevação corresponde ao valor da equidistância natural.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "curva estimada ou interpolada a partir das curvas de nível circundantes. utilizada em áreas onde a informação altimétrica é insuficiente.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "distrito": {
        "tema": "unidades administrativas",
        "geo_caract": "distrito",
        "def": "divisão administrativa estabelecida de acordo com a constituição portuguesa. ",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (multipolígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "data_publicacao",
                "mult": "1",
                "tipo": "Data",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "di",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": []
    },
    "constru_polig": {
        "tema": "construções",
        "geo_caract": "construção poligonal",
        "def": "construção poligonal não classificada como “edifício”.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_construcao",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_construcao",
                "vals": [
                    {
                        "val": "1",
                        "def": "paredão ou muro de grande grossura que avança pela água dentro, geralmente à entrada de um porto ou perpendicular a praias, para quebrar o ímpeto das ondas, abrigar navios, servir de atracadouro ou para reter as areias das praias.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "construção que entra pelo mar, curso ou massa de água adentro. local específico no cais ou ponte-cais, onde um navio pode acostar ou amarrar para proceder a operações de embarque ou desembarque de carga ou passageiros.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "reservatório feito de pedra, cimento ou outro material, onde se armazena água para fins lúdicos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "reservatório feito de pedra, cimento ou outro material, onde se armazena água para fins diversos como rega, lavagem de roupa ou para bebedouro de animais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "limite do campo de jogos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "pequena extensão de água que cobre uma área com fundo impermeável e tem fins decorativos ou recreativos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "elemento arquitetónico não relacionado com vias de comunicação nem com edifício residencial.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "conjunto de assentos dispostos em filas sucessivas, cada uma num nível superior ao da anterior (existe em estádios ou anfiteatros).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "área que ladeia a via rodoviária e que se destina à circulação de peões.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "10",
                        "def": "limite das construções delimitadoras definidas na “construção linear” (exemplo: muros, muralhas e sebes).",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "constru_linear": {
        "tema": "construções",
        "geo_caract": "construção linear",
        "def": "eixo da construção com forma linear.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_construcao_linear",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "largura",
                "mult": "1",
                "tipo": "Real",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "suporte",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_construcao_linear",
                "vals": [
                    {
                        "val": "1",
                        "def": "obra em alvenaria ou em betão que delimita ou separa áreas ou estruturas distintas, com altura e largura variáveis.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "obra de pedras sobrepostas, com ou sem argamassa, que delimita ou separa áreas ou estruturas distintas, com altura e largura variáveis (inclui muros de gabiões).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "tapumes feitos de arbustos emaranhados, árvores justapostas, ramos secos, varas entrelaçadas, para vedarem ou cercarem áreas ou separarem outras estruturas, sobre solo, muros ou muretes ou sobre pequenas valas.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "delimitação física com o objetivo de impedir a passagem de pessoas, animais ou veículos, formada por barras de metal, arame ou ripas de madeira.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "muro de grande espessura e geralmente bastante elevado, construído, como obra defensiva, à volta de uma fortaleza, de uma praça de armas ou que protege um território.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "porta que pode ter diversos tamanhos e formas e que, geralmente fecha uma abertura num muro ou numa grade, impedindo o acesso da via pública a um local privado.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "7",
                        "def": "dispositivo, normalmente sob a forma de painel, limitador da propagação de som, colocado entre as habitações e as estradas ou entre habitações e vias férreas.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "8",
                        "def": "circuito para competição desportiva diversa (atletismo e desportos motorizados).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "elemento, geralmente longo e estreito, de pedra ou cimento que, por vezes, forma o bordo de um passeio ou calçada.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "equip_util_coletiva": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "equipamento de utilização coletiva",
        "def": "equipamento para prestação de serviços de interesse coletivo.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "valor_tipo_equipamento_coletivo",
                "mult": "[1..*]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "ponto_de_contacto",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_equipamento_coletivo",
                "vals": [
                    {
                        "val": "1",
                        "def": "equipamento dedicado às questões da educação ou da investigação.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "1.1",
                        "def": "equipamento de acolhimento de crianças até aos cinco anos de idade.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.2",
                        "def": "equipamento onde se ministra qualquer nível de escolaridade obrigatória.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.3",
                        "def": "equipamento onde se ministra o ensino superior ou onde se desenvolve a pesquisa científica e tecnológica.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.4",
                        "def": "equipamento onde se prestam serviços de apoio às atividades de educação ou investigação (ex. reitoria, cantina, biblioteca, residência, etc.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.5",
                        "def": "equipamento de ensino para adultos e outras atividades educativas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "equipamento dedicado às questões da saúde.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.1",
                        "def": "equipamento dotado de capacidade de internamento, de ambulatório ou de meios de diagnóstico e terapêutica, com o objetivo de prestar à população assistência médica curativa e de reabilitação.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2.2",
                        "def": "equipamento de uma unidade básica do serviço nacional de saúde para atendimento e prestação de cuidados de saúde à população.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2.3",
                        "def": "equipamento onde se exercem atividades de saúde como centros de reabilitação, sanatórios, clínicas, farmácias, etc. (inclui serviços veterinários).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "equipamento dedicado aos serviços de proteção social (inclui a prestação de serviços de apoio à infância, ao idoso e à pessoa com deficiência).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "equipamento dedicado aos serviços de segurança e ordem pública.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4.1",
                        "def": "equipamento de controlo ou execução de operações dos serviços de proteção civil ou onde se aloja um corpo de bombeiros e respetivos meios técnicos de intervenção.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4.2",
                        "def": "equipamento dedicado à missão de proteger e garantir a ordem e a segurança pública (inclui edifício da policia de segurança pública e da guarda nacional republicana, as instalações da policia judiciária, a policia municipal, a policia marítima e os postos fronteiriços).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "equipamento dedicado aos serviços de defesa militar do estado (inclui quartel, estado maior, capitania ou outros de apoio aos serviços prestados no âmbito da defesa militar.).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "equipamento dedicado aos serviços de aplicação do direito e das leis na resolução de litígios, julgamentos de causas, atribuição de penas.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.1",
                        "def": "equipamento onde se discutem, julgam e decidem questões forenses e do contencioso administrativo, onde se fazem julgamentos, onde se ministra a justiça, assegurando a defesa dos direitos e interesses juridicamente protegidos dos cidadãos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6.2",
                        "def": "equipamento onde as pessoas são encerradas e mantidas privadas de liberdade por força de ordem de prisão preventiva ou por cumprimento de pena de prisão.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "equipamento de desporto e lazer.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7.1",
                        "def": "equipamento que inclui parque e jardim.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.2",
                        "def": "equipamento normalmente em contexto urbano.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.3",
                        "def": "equipamento para a prática de golfe.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.4",
                        "def": "outro equipamento para desporto e lazer.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "equipamento de inumação.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "elem_assoc_eletricidade": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "elemento associado de electricidade",
        "def": "infraestrutura associada à transformação e distribuição de energia elétrica.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_elemento_associado_electricidade",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_elemento_associado_electricidade",
                "vals": [
                    {
                        "val": "1",
                        "def": "infraestrutura que contém instalação ou instalações de produção de energia.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "1.1",
                        "def": "infraestrutura que contém uma instalação de produção elétrica a partir de uma barragem.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.2",
                        "def": "infraestrutura que contém uma instalação de produção de energia elétrica a partir da radiação emitida pelo sol.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.3",
                        "def": "infraestrutura que contém uma instalação de produção de energia elétrica a partir da energia cinética dos ventos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1.4",
                        "def": "infraestrutura que contém uma instalação de produção de energia elétrica a partir de energia libertada em forma de calor.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "instalação de rede elétrica onde confluem diversas linhas elétricas, dotada de equipamento de corte, seccionamento, medida, controlo, proteção e transformação de tensão.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "engenho para produzir energia elétrica quando acionado pela energia do vento.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "equipamento de grande porte capaz de transformar a energia do vento em energia elétrica que é inserida diretamente na rede elétrica.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "equipamento utilizado para converter a energia solar em energia elétrica quando não estão sobre edifícios nem incluídos em centrais fotovoltaicas.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "6",
                        "def": "estrutura de iluminação coincidente, ou não, com apoio de baixa tensão.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6.1",
                        "def": "haste vertical, colocada no solo ou fixada em alvenaria, para suporte de luminárias de iluminação da via pública.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "6.2",
                        "def": "estrutura metálica reticulada ou de betão que suporta os cabos condutores, os cabos de guarda, os isoladores e os acessórios de fixação das linhas elétricas de tensão até 1kv e que coincide com suporte de iluminação vertical da via pública.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "7",
                        "def": "elemento metálico ou de betão de uma linha aérea destinado a suportar os cabos condutores, os cabos de guarda, os isoladores e os acessórios de fixação.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7.1",
                        "def": "estrutura metálica reticulada, tubular ou de betão que suporta os cabos condutores, os cabos de guarda, os isoladores e os acessórios de fixação das linhas elétricas de tensão superior a 45kv e menor ou igual a 110kv.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7.2",
                        "def": "estrutura metálica reticulada, tubular ou de betão que suporta os cabos condutores, os cabos de guarda, os isoladores e os acessórios de fixação das linhas elétricas de tensão superior a 1kv e menor ou igual a 45kv.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "7.3",
                        "def": "estrutura metálica reticulada ou de betão que suporta os cabos condutores, os cabos de guarda, os isoladores e os acessórios de fixação das linhas elétricas de tensão até 1kv.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "7.4",
                        "def": "estrutura metálica reticulada ou tubular que suporta os cabos condutores, os cabos de guarda, os isoladores e os acessórios de fixação das linhas elétricas de tensão superior a 110kv.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "instalação ou equipamento sobre o solo, destinado à transformação da corrente elétrica.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "zona_humida": {
        "tema": "hidrografia",
        "geo_caract": "zona húmida",
        "def": "área mal drenada ou periodicamente inundada em que o solo se encontra saturado de água e onde cresce vegetação.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_zona_humida",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "mare",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_zona_humida",
                "vals": [
                    {
                        "val": "1",
                        "def": "solo alagadiço, geralmente localizado nas margens de rios e cuja cobertura varia com a quantidade de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "zona húmida, constituídas por solos mal drenados, geralmente impermeáveis, o que provoca o seu encharcamento e anoxia (comum no interior dos açores).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "terreno alagado com água estagnada (pântano). é um tipo de ecossistema lagunar que se inclui na categoria de zonas húmidas palustres.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "mob_urbano_sinal": {
        "tema": "mobiliário urbano e sinalização",
        "geo_caract": "mobiliário urbano e sinalização",
        "def": "equipamento de utilidade pública destinado à prestação de serviços necessários ao funcionamento dos aglomerados urbanos.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_mob_urb_sinal",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_mob_urb_sinal",
                "vals": [
                    {
                        "val": "1",
                        "def": "armário existente na via pública, nomeadamente em passeios, utlizado para apoio ao funcionamento de um ou vários semáforos.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "assento estreito e comprido, de material variável, com ou sem encosto, para várias pessoas, localizado em jardim ou área de lazer.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "espaços de terreno, de pequena dimensão, normalmente retangulares, em que se plantam flores ou quaisquer plantas ou relvas.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "recetáculo de dimensões normalizadas que se destina à recolha de lixo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "área onde se encontram localizados um ou vários contentores para recolha de substâncias diversas, como lixo orgânico, vidros, plásticos ou produtos químicos bem identificados.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "aparelhos de exercício físico ao ar livre instalados em parques e jardins.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "7",
                        "def": "área própria para estacionar velocípedes.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "8",
                        "def": "pequena construção, geralmente cilíndrica, ou outro tipo de dispositivo, com uma ranhura, localizada na via pública, que serve de recetáculo público à correspondência postal, antes de esta ser encaminhada para a estação de correios pelos serviços competentes.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "9",
                        "def": "estrutura de divulgação publicitária colocada na via pública.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "10",
                        "def": "pequeno dispositivo localizado na via pública, normalmente instalado em suportes tipo poste, e para funcionar como recetáculo de papéis e outro tipo de lixo.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "11",
                        "def": "área, normalmente ao ar livre, especialmente destinada a crianças.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "12",
                        "def": "dispositivo localizado na via pública, ou na entrada de parques de estacionamento descobertos, onde o utente deverá efetuar o pagamento da respetiva taxa.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "13",
                        "def": "faixa, geralmente listrada de branco, que atravessa transversalmente uma rua ou outro tipo de arruamento e que se destina ao trânsito de peões.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "14",
                        "def": "dispositivo de dimensão variável com indicações institucionais, turísticas ou de direção.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "15",
                        "def": "galeria, balcão ou terraço afastado da parede, com pilares que suportam barrotes que podem ser cobertos por trepadeiras, toldos, etc.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "16",
                        "def": "ponto de carregamento de eletricidade para a mobilidade elétrica.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "17",
                        "def": "pavilhão pequeno, de madeira, ferro, alumínio ou de cimento, situado geralmente nos aglomerados urbanos, ou em zonas de grande afluência de público, destinado à venda de jornais, tabacos, bebidas e outras miudezas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "18",
                        "def": "instalação ou conjunto de instalações públicas próprias para higiene pessoal ou para banho.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "19",
                        "def": "estrutura de suporte de um sinal luminoso, constituído, geralmente, por luzes verde, amarelo e vermelho, que acendem de forma alternada e que se encontra colocado na via pública em cruzamentos ou em determinados pontos das vias rodoviárias ou ferroviárias, para regular o fluxo de tráfego.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "20",
                        "def": "sinal que, devido à sua localização, forma, cor, tipo ou ainda símbolo e/ou caracteres alfanuméricos, transmite aos condutores uma mensagem visual com um determinado significado.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "21",
                        "def": "sinal que transmite aos condutores uma mensagem visual.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "ponto_interesse": {
        "tema": "construções",
        "geo_caract": "ponto de interesse",
        "def": "fenómeno do mundo real a que corresponde uma localização específica com especial relevância.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_ponto_interesse",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_ponto_interesse",
                "vals": [
                    {
                        "val": "1",
                        "def": "pequeno monumento na berma dos caminhos ou das estradas assinalando à pessoas que passam as almas do purgatório ou lembrando a memória de pessoas que ali tenham falecido.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "monumento sepulcral pré-histórico, delimitado por pedras mais ou menos verticais, esteios, e coberto por uma grande laje horizontal, mesa ou chapéu.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "árvore de qualquer tipo quando não localizada em parques, jardins, áreas desportivas ou áreas agrícolas, florestais ou matos.",
                        "d1": "x",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "árvore de valor patrimonial elevado (classificação atribuída pelo instituto da conservação da natureza e das florestas).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "corresponde a ruínas ou restos arqueológicos de um tipo de povoado da idade do cobre e da idade do ferro, característico de zonas da península ibérica.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "cruz de pedra, raramente de madeira, colocada em praças, adros de igrejas, em caminhos e encruzilhadas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "escultura em três dimensões representativa de uma figura humana, animal, mítica ou divina.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "8",
                        "def": "monumento megalítico que consiste numa pedra mais ou menos cilíndrica fixada verticalmente no solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "9",
                        "def": "local elevado onde se pode observar e contemplar um largo horizonte.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "10",
                        "def": "monumento de pedra semelhante a um marco que os navegadores portugueses erguiam nas terras por eles descobertas ou monumento monolítico erigido para comemorar um dado acontecimento.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "11",
                        "def": "coluna de pedra, símbolo do poder judicial de um concelho, erigida em praça ou sítio central e pública, junto do qual se expunham e castigavam os transgressores.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "12",
                        "def": "resto ou destroço de um ou mais edifícios ou construções de raiz histórica.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "13",
                        "def": "outro não contemplado nas opções anteriores e que tenha relevância para a caracterização do território.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "no_trans_ferrov": {
        "tema": "transportes (transporte ferroviário)",
        "geo_caract": "nó de transporte ferroviário\n",
        "def": "ponto utilizado para representar a conectividade entre segmentos da via-férrea, estabelecer a relação com outras vias de comunicação e com as infraestruturas associadas à ferrovia.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_no_trans_ferrov",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_no_trans_ferrov",
                "vals": [
                    {
                        "val": "1",
                        "def": "nó que identifica a interseção ao mesmo nível de 3 ou mais segmentos da via ferroviária.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "nó que identifica a interseção ao mesmo nível do segmento da via ferroviária com o segmento da via rodoviária.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "nó que representa um ponto que une dois segmentos da via ferroviária com características distintas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "nó que identifica o fim do segmento da via ferroviária (não existe conetividade com outro segmento).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "nó que identifica a presença de uma infraestrutura de transporte ferroviário.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "no_hidrografico": {
        "tema": "hidrografia",
        "geo_caract": "nó hidrográfico",
        "def": "ponto utilizado para representar a conectividade entre segmentos da rede hidrográfica.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_no_hidrografico",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_no_hidrografico",
                "vals": [
                    {
                        "val": "1",
                        "def": "nó que identifica o início de uma série de linhas de “curso de água - eixo”.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "nó que identifica o final de uma série de linhas de “curso de água - eixo”.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "nó que identifica a intersecção de 3 ou mais linhas de “curso de água - eixo”.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "nó que representa um ponto que une 2 linhas “curso de água- eixo” com características distintas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "nó que representa um local que condiciona o fluxo do curso de água (ex: “queda de água”; “zona húmida”).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "nó que representa um local que regula o fluxo do curso de água. (ex: “barreira”).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "nó usado para conectar redes diferentes. o curso de água atravessa uma fronteira.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "adm_publica": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "administração pública e órgãos de soberania",
        "def": "serviço da administração pública e órgãos de soberania.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "valor_tipo_adm_publica",
                "mult": "[1]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "nome",
                "mult": "1",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "ponto_de_contacto",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_tipo_adm_publica",
                "vals": [
                    {
                        "val": "1",
                        "def": "edifício onde os representantes do povo exercem o poder democrático a nível nacional ou regional.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "edifício onde se dirigem e exercem atividades administrativas da responsabilidade da administração central do estado.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "edifício onde se exercem atividades de gestão municipal ou onde os representantes dos munícipes exercem o poder democrático autárquico.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "edifício onde se dirigem e exercem atividades administrativas da responsabilidade da junta de freguesia e da assembleia de freguesia.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "edifício onde se exercem atividades da responsabilidade do estado tais como direção geral, instituto público, agência pública, loja do cidadão, etc.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "barreira": {
        "tema": "hidrografia",
        "geo_caract": "barreira",
        "def": "barreira permanente num curso de água utilizada para reter a água ou para controlar o seu caudal.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_barreira",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_barreira",
                "vals": [
                    {
                        "val": "1",
                        "def": "porta que sustem as águas de diques, açudes, represas ou barragens e que pode abrir-se para o seu escoamento.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "obra hidráulica, construída sobre um rio ou em zona portuária, com uma ou várias comportas, que permite a navegação quando é necessário vencer grandes desníveis.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "barreira numa barragem de betão que retém a água de um curso de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "barreira numa barragem de terra que retém a água de um curso de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "pequena barreira artificial ou natural que retém a água de um curso de água.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "obra hidráulica, construída para desviar ou conter a invasão da água do mar ou de um curso de água.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "nascente": {
        "tema": "hidrografia",
        "geo_caract": "nascente",
        "def": "ponto da superfície do solo (a céu aberto) de onde brota água ou nasce um curso de água.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_persistencia_hidrologica",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "valor_tipo_nascente",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "id_hidrografico",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "nome",
                "mult": "[0..1]",
                "tipo": "Texto",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_persistencia_hidrologica",
                "vals": [
                    {
                        "val": "1",
                        "def": "cheio e/ou a correr raramente, de um modo geral, unicamente durante e /ou logo após forte precipitação.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "cheio e/ou a correr durante e logo após precipitações.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "cheio e/ou a correr durante uma parte do ano.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "cheio e/ou a correr continuamente ao longo do ano.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_tipo_nascente",
                "vals": [
                    {
                        "val": "1",
                        "def": "ponto da superfície do solo de onde brota água ou onde nasce um curso de água.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "ponto da superfície do solo de onde brota água mineral.",
                        "d1": "",
                        "d2": ""
                    }
                ]
            }
        ]
    },
    "seg_via_rodov": {
        "tema": "transportes (transporte rodoviário)",
        "geo_caract": "segmento da via rodoviária",
        "def": "eixo da faixa de rodagem da via rodoviária.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (linha)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_estado_via_rodov",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_caract_fisica_via_rodov",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_posicao_vertical_transportes",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_restricao_acesso",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "valor_sentido",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_circulacao",
                "mult": "[1..*]",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_tipo_troco_rodoviario",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "gestao",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "jurisdicao",
                "mult": "1",
                "tipo": "Texto",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "multipla_faixa_rodagem",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "num_vias_transito",
                "mult": "1",
                "tipo": "Inteiro",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "pavimentado",
                "mult": "1",
                "tipo": "Booleano",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "velocidade_max",
                "mult": "1",
                "tipo": "Inteiro",
                "d1": "",
                "d2": ""
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_estado_via_rodov",
                "vals": [
                    {
                        "val": "1",
                        "def": "a instalação já não é utilizada e foi ou está a ser desmantelada.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "a instalação encontra-se em fase de construção e ainda não está funcional. este valor aplica-se unicamente à construção inicial da instalação e não a trabalhos de manutenção.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "a instalação já não é utilizada, mas não está a ser ou não foi desmantelada.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "a instalação encontra-se em fase de projeto. os trabalhos de construção ainda não tiveram início.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "a instalação está a ser utilizada (aberta ao tráfego).",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_caract_fisica_via_rodov",
                "vals": [
                    {
                        "val": "1",
                        "def": "via destinada a trânsito rápido, com separação física de faixas de rodagem, sem cruzamentos de nível nem acesso a propriedades marginais, com acessos condicionados e sinalizadas como tal.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "via para circulação automóvel com percurso predominantemente não urbano, constituído por faixa de rodagem e bermas e que estabelece ligação com vias urbanas e rurais.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "via para circulação automóvel, ciclistas e peões dentro de localidades.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "via habitualmente em meio rural (inclui caminho vicinal).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "via que desempenha funções de corta-fogo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "via de comunicação destinada à circulação de velocípedes podendo permitir a circulação de pessoas a pé.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_posicao_vertical_transportes",
                "vals": [
                    {
                        "val": "3",
                        "def": "a entidade geográfica está assente no terceiro ou superior nível acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "a entidade geográfica está assente no segundo nível acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "1",
                        "def": "a entidade geográfica está assente no primeiro nível acima do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "0",
                        "def": "a entidade geográfica está assente diretamente no solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-1",
                        "def": "a entidade geográfica está assente no primeiro nível abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-2",
                        "def": "a entidade geográfica está assente no segundo nível abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "-3",
                        "def": "a entidade geográfica está assente no terceiro ou inferior nível abaixo do solo.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_restricao_acesso",
                "vals": [
                    {
                        "val": "1",
                        "def": "sem restrições de acesso.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2",
                        "def": "sujeito a cobrança de portagem.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "3",
                        "def": "condicionado a decisão do proprietário.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "4",
                        "def": "sujeito a disposição legal.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "5",
                        "def": "restrição de caracter temporário.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "6",
                        "def": "acesso fisicamente impossível devido à existência de barreiras ou outros obstáculos físicos. ",
                        "d1": "",
                        "d2": ""
                    }
                ]
            },
            {
                "nome": "valor_sentido",
                "vals": [
                    {
                        "val": "1",
                        "def": "a circulação decorre nos dois sentidos.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "a circulação apenas ocorre num único sentido que é o sentido da ordem dos vértices do objeto.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "a circulação apenas ocorre num único sentido que é o sentido contrário ao da ordem dos vértices do objeto.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_circulacao",
                "vals": [
                    {
                        "val": "1",
                        "def": "permite a circulação de veículos ligeiros ou pesados.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "permite a circulação de veículos agrícolas ou de veículos com tração às quatro rodas.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "dedicada à circulação de velocípedes.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "dedicada à circulação de pessoas a pé.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            },
            {
                "nome": "valor_tipo_troco_rodoviario",
                "vals": [
                    {
                        "val": "1",
                        "def": "troço principal da via rodoviária.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "via rodoviária especialmente concebida para entrada e saída de outra via rodoviária, incluindo vias de aceleração e desaceleração.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "praça comum de confluência de duas ou mais vias públicas, sinalizada como tal, onde o trânsito se reúne e distribui, circulando numa faixa de rodagem de sentido único em torno de uma placa central.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "4",
                        "def": "via de acesso destinada a prestar apoio a infraestruturas ou equipamentos, habitualmente de acesso condicionado. inclui caminho paralelo e via coletora.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "5",
                        "def": "via em degraus dispostos em plano inclinado.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "6",
                        "def": "caminho rudimentar, vereda, carreiro ou atalho.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "7",
                        "def": "passagem estreita para peões e/ou velocípedes construída sobre o terreno ou suspensa/elevada sobre um curso de água, via de comunicação ou depressão do terreno.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    },
    "elem_assoc_p_g_q": {
        "tema": "infraestruturas e serviços de interesse público",
        "geo_caract": "elemento associado de petróleo, gás e substâncias químicas",
        "def": "infraestrutura de armazenamento ou distribuição de produtos químicos.",
        "attr": [
            {
                "nome": "identificador",
                "mult": "1",
                "tipo": "UUID",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "inicio_objeto",
                "mult": "1",
                "tipo": "DataTempo",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "fim_objeto",
                "mult": "[0..1]",
                "tipo": "DataTempo",
                "d1": "",
                "d2": ""
            },
            {
                "nome": "geometria",
                "mult": "1",
                "tipo": "Geometria (ponto; polígono)",
                "d1": "x",
                "d2": "x"
            },
            {
                "nome": "valor_elemento_associado_p_g_q",
                "mult": "1",
                "tipo": "Lista de códigos",
                "d1": "x",
                "d2": "x"
            }
        ],
        "lst_cod": [
            {
                "nome": "valor_elemento_associado_p_g_q",
                "vals": [
                    {
                        "val": "1",
                        "def": "classificação de elementos associados de redes de petróleo ou derivados.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2",
                        "def": "classificação de elementos associados de redes de gás.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.1",
                        "def": "instalação de superfície destinada à manobra do fluxo de gás natural transportado na rede nacional de transporte de gás natural (rntgn).",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2.1.1",
                        "def": "instalação de superfície destinada ao seccionamento da rede de transporte de gás natural em intervalos de segurança definidos ao abrigo da legislação em vigor.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.1.2",
                        "def": "instalação de superfície destinada à junção / derivação de duas ou mais linhas de gasodutos.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.1.3",
                        "def": "instalação de superfície destinada à redução de pressão e medição de gás para ligação às redes de distribuição de gás natural.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.1.4",
                        "def": "instalação de superfície destinada a interligar grandes consumidores de gás natural diretamente à rntgn.",
                        "d1": "",
                        "d2": ""
                    },
                    {
                        "val": "2.2",
                        "def": "instalação que inclui uma unidade de superfície para injeção e extração de gás natural.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2.3",
                        "def": "instalação que inclui as unidades de gás natural liquefeito (gnl) dos navios metaneiros, os depósitos de gnl e as unidades de regaseificação de gás natural.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "2.4",
                        "def": "instalação de superfície destinada ao armazenamento e regaseificação de gás natural liquefeito (gnl) transportado por via terrestre, em regiões não servidas por rede de gasodutos de transporte.",
                        "d1": "x",
                        "d2": "x"
                    },
                    {
                        "val": "3",
                        "def": "classificação de elementos associados de redes de substâncias químicas.",
                        "d1": "x",
                        "d2": "x"
                    }
                ]
            }
        ]
    }
}
